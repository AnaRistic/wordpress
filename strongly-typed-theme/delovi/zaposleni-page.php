<?php /* Template Name: Zaposleni */ ?>

<div id="banner-wrapper">
	<div class="inner">
		<section id="banner" class="container">
			<p><?php the_content(); ?></p>
			<h3><?php the_title(); ?></h3>
		</section>
	</div>
</div>