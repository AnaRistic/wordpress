<?php
if ( is_home() ) :
    get_header( 'home' );
else :
    get_header();
endif;
?>

<!-- Main -->
<div id="main-wrapper">
	<div id="main" class="container">
		<div class="row">

			<!-- Content -->
				<div id="content" class="8u 12u(mobile) important(mobile)">

				<!-- Post -->
					<article class="box post">

						<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

							<header>
								<h2><?php the_title(); ?></h2>
							</header>

							<?php
								if (has_post_thumbnail()) {
									the_post_thumbnail( 'clanak-slika', array('class' => 'image featured') );
								}
							?>

							<p><?php the_content(); ?></p>

						<?php endwhile; endif; ?>

					</article>

				</div>

			<!-- Sidebar -->
				<div id="sidebar" class="4u 12u(mobile)">

					<!-- Excerpts -->
						<section>
							<ul class="divided">
								<li>

									<!-- Excerpt -->
										<article class="box excerpt">
											<?php dynamic_sidebar( 'rsidebar' ); ?>
										</article>

								</li>
							</ul>
						</section>
				</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>