<?php



function wpa_theme_setup() {

	// This theme styles the visual editor with editor-style.css to match the theme style.

	add_editor_style();

	

	// Adds RSS feed links to <head> for posts and comments.


	add_theme_support('automatic-feed-links');

	
	// This theme uses wp_nav_menu() in one location.

	register_nav_menu('head-nav', 'main navigation');

    

   	// This theme uses a custom image size for featured images.

	add_theme_support('post-thumbnails');

	set_post_thumbnail_size(771, 350, true);

	add_image_size('istaknuta-slika', 355, 238, true);

	add_image_size('clanak-slika', 1188, 459, array( 'left', 'top' ));

	

}



add_action('after_setup_theme', 'wpa_theme_setup');

    // This feature add Theme Support for HTML 5 Figures and Figcaptions to WordPress Images

	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

    // This feature allows themes to add document title tag to HTML <head>.

	add_theme_support( 'title-tag' );

    //  This tells WordPress which post formats to support by passing an array of formats

	add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );




/** Creates a nicely formatted and more specific title element text. **/

    // This filters the text appearing in the HTML <title> tag (sometimes called the “title tag” or “meta title”), not the post, page, or category title.

function wpa_wp_title($title, $sep) {

	global $paged, $page;

	// Title for feed.

	if(is_feed()) {

		return $title;

	}

	// Add the site name.

	$title .= get_bloginfo('name');

	

	return $title;

}



	add_filter('wp_title', 'wpa_wp_title', 10, 2);





/**

 * Register our sidebars and widgetized areas.

 *

 */

function wpa_widgets_init() {



	register_sidebar( array(

		'name' => 'Sidebar',

		'id' => 'sidebar',

		'before_widget' => '<article class="box excerpt">',

		'after_widget' => '</article>',

	) );

	register_sidebar( array(

		'name' => 'Highlight Sidebar',

		'id' => 'highlight-sidebar',

		'before_widget' => '<article class="box excerpt">',

		'after_widget' => '</article>',

	) );

    

   	register_sidebar( array(

		'name' => 'Footer levi',

		'id' => 'footer-levi',

        'before_widget' => '<div>',

		'before_widget' => '<section>',

        'after_widget' => '</section>',

		'after_widget' => '</div>',


	) );

	register_sidebar( array(

		'name' => 'Footer desni',

		'id' => 'footer-desni',

        'before_widget' => '<div>',

		'before_widget' => '<section>',

        'after_widget' => '</section>',

		'after_widget' => '</div>',


	) );

	register_sidebar( array(

		'name' => 'Quote sadrzaj',

		'id' => 'quote',

        'before_widget' => '<div class="inner">',

		'before_widget' => '<section id="banner" class="container">',

        'after_widget' => '</section>',

		'after_widget' => '</div>',


	) );

	register_sidebar( array(

		'name' => 'Kontakt',

		'id' => 'kontakt',

		'before_widget' => '<article class="box excerpt">',

		'after_widget' => '</article>',

	) );

	register_sidebar( array(

		'name' => 'Right Sidebar',

		'id' => 'rsidebar',

		'before_widget' => '<article class="box excerpt">',

		'after_widget' => '</article>',

	) );

	register_sidebar( array(

		'name' => 'Left Sidebar',

		'id' => 'lsidebar',

		'before_widget' => '<article class="box excerpt">',

		'after_widget' => '</article>',

	) );

	
}

add_action( 'widgets_init', 'wpa_widgets_init' );



if ( ! isset( $content_width ) ) {

	$content_width = 780;

}



/** Enqueues styles and scripts for admin panel. **/



function wpa_admin_enqueues_styles_scripts() {

	global $wp_styles;

	

	// Get theme version.

	$wp_theme_version = esc_attr(wp_get_theme()->Version);

	

	// Load stylesheets.

	wp_enqueue_style('wpa-admin', get_template_directory_uri().'/css/admin.css', array(), $wp_theme_version, 'all');

    wp_enqueue_style('wpa-style', get_template_directory_uri().'/css/style.css', array(), $wp_theme_version, 'all');

	

	}





add_action('admin_enqueue_scripts', 'wpa_admin_enqueues_styles_scripts');


function loading_theme_full(){
	

	wp_enqueue_style( 'wpcs-main', get_template_directory_uri() . '/assets/css/main.css' );
	wp_enqueue_style('wpcs-ie8',get_template_directory_uri() . '/assets/css/ie8.css');
	wp_enqueue_style('wpcs-fonts',get_template_directory_uri().'/assets/css/font-awesome.min.css');
	
	wp_enqueue_script( 'fn', get_template_directory_uri() . '/assets/js/jquery.dropotron.min.js', array('jquery'),'', true );
	wp_enqueue_script( 'fn', get_template_directory_uri() . '/assets/js/jquery.min.js', array('jquery'),'', true );
	wp_enqueue_script( 'fn', get_template_directory_uri() . '/assets/js/main.js', array('jquery'),'', true );
	wp_enqueue_script( 'fn', get_template_directory_uri() . '/assets/js/skel.min.js', array('jquery'),'', true );
	wp_enqueue_script( 'fn', get_template_directory_uri() . '/assets/js/skel-viewport.min.js', array('jquery'),'', true );
	wp_enqueue_script( 'fn', get_template_directory_uri() . '/assets/js/util.js', array('jquery'),'', true );

}
add_action( 'wp_enqueue_scripts', 'loading_theme_full' );

    // This feature returns the excerpt of the post. This is either a user-supplied excerpt, that is returned unchanged

function the_excerpt_max_charlength( $charlength ) {

	$excerpt = get_the_excerpt();

	$charlength++;



	if ( mb_strlen( $excerpt ) > $charlength ) {

		$subex = mb_substr( $excerpt, 0, $charlength - 5 );

		$exwords = explode( ' ', $subex );

		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );

		if ( $excut < 0 ) {

			echo mb_substr( $subex, 0, $excut );

		} else {

			echo $subex;

		}

		echo '[...]';

	} else {

		echo $excerpt;

	}

}

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

// Odd Even Posts
function oddeven_post_class ( $classes ) {
   global $current_class;
   $classes[] = $current_class;
   $current_class = ($current_class == 'odd') ? 'even' : 'odd';
   return $classes;
}
add_filter ( 'post_class' , 'oddeven_post_class' );
global $current_class;
$current_class = 'odd';

// Deactivate XML RPC
add_filter('xmlrpc_enabled', '__return_false');

// Remove default WP Footer
function remove_footer_admin () {

echo '<p>Napravljeno u <a href="http://www.wordpress.org" target="_blank">WordPress</a> | Nauči WordPress: <a href="http://www.wpakademija.rs" target="_blank">WPAkademija</a></p>';

}

add_filter('admin_footer_text', 'remove_footer_admin');



?>