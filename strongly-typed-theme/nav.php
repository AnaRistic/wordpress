<?php

	$defaults = array(
		'container' => 'nav',
		'container_id' => 'nav',
		'echo' => 'true',
		'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'depth' => 0,
	);

	wp_nav_menu( $defaults );

?>