<?php /* Template Name: Home */ ?>

<!DOCTYPE HTML>
<!--
	Strongly Typed by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title><?php bloginfo('name'); ?></title>
		<meta charset="<?php bloginfo('charset'); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class('homepage'); ?>>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<div id="header" class="container">

						<!-- Logo -->
							<h1 id="logo"><a href="<?php echo esc_url( home_url() ) ?>"><?php bloginfo( 'name' ); ?></a></h1>
							<p><?php bloginfo( 'description' ); ?></p>

						<!-- Nav -->
							<?php

								get_template_part('nav');
								
							?>

					</div>
				</div>