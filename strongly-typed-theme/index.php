<?php get_header('home'); ?>
<!-- Features -->
	<div id="features-wrapper">
		<section id="features" class="container">
			<header>
				<h2>Gentlemen, behold! This is <strong>Strongly Typed</strong>!</h2>
			</header>
			<div class="row">
				<?php 
				$args = array(
					'posts_per_page'   => 3,
					'category_name'    => 'istaknuti', 
				);
				$the_query1 = new WP_Query( $args ); ?>
				<?php while ($the_query1 -> have_posts()) : $the_query1 -> the_post(); ?>
					<div class="4u 12u(mobile)">
						<section>
							<a href="<?php the_permalink(); ?>" class="image featured">
								<?php
					                if ( has_post_thumbnail() ) {
					                the_post_thumbnail('istaknuta-slika', array('class' => 'image featured'));
					                }
				                ?>
							</a>
							<header>
								<h3><?php the_title(); ?></h3>
							</header>
							<p><?php the_excerpt_max_charlength(80); ?></p>
						</section>
					</div>
				<?php endwhile;?>
				<?php wp_reset_query(); ?>
			</div>
			<ul class="actions">
				<li><a href="<?php echo esc_url( home_url() ); ?>" class="button icon fa-file">Tell Me More</a></li>
			</ul>
		</section>
	</div>

<!-- Banner -->
	<div id="banner-wrapper">
		<div class="inner">
			<?php dynamic_sidebar( 'quote' ); ?>
		</div>
	</div>

<!-- Main -->
	<div id="main-wrapper">
		<div id="main" class="container">
			<div class="row">

				<!-- Content -->
					<div id="content" class="8u 12u(mobile)">

						<!-- Post -->
							<article class="box post">
								<?php
								$args2 = array(
									'posts_per_page' => 2,
								);
								$the_query2 = new WP_Query( $args2 );
								?>
								<?php while ($the_query2 -> have_posts()) : $the_query2 -> the_post(); ?>
									<header>
										<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									</header>
									<a href="<?php the_permalink(); ?>" class="image featured">
										<?php
											if ( has_post_thumbnail() ) {
												the_post_thumbnail( 'istaknuta-slika');
											}
										?>
									</a>
									<h3><?php the_title(); ?></h3>
									<p><?php the_excerpt(); ?></p>
									<ul class="actions">
										<li><a href="<?php the_permalink(); ?>" class="button icon fa-file">Continue Reading</a></li>
									</ul>
								<?php endwhile;?>
								<?php wp_reset_query(); ?>
							</article>

					</div>

					<?php get_sidebar(); ?>
					
			</div>
		</div>
	</div>
 

<?php //get_template_part( 'delovi/zaposleni', 'page' ); ?>

<?php get_footer(); ?>
