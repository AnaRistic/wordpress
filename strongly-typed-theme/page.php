<?php
if ( is_home() ) :
    get_header( 'home' );
else :
    get_header();
endif;
?>

<div id="main-wrapper">
	<div id="main" class="container">
		<div id="content">

		<!-- Post -->
			<article class="box post">

				<?php if(have_posts()): while(have_posts()): the_post(); ?>

				<header>
					<h2><?php the_title(); ?></h2>
				</header>
				
					<?php
	                	if ( has_post_thumbnail() ) {
	                		the_post_thumbnail('clanak-slika', array('class' => 'image featured'));
	                	}
	                ?>

					<p><?php the_content(); ?></p>

			<?php endwhile; endif; ?>

			</article>

		</div>
	</div>
</div>

<?php get_footer(); ?>