
	<!-- Highlights -->	<!-- Sidebar -->
<div id="sidebar" class="4u 12u(mobile)">

	<!-- Excerpts -->
		<section>
			<ul class="divided">
				<li>

					<!-- Excerpt -->
						<article class="box excerpt">
							<?php dynamic_sidebar( 'sidebar' ); ?>
						</article>

				</li>									
			</ul>
		</section>

	<!-- Highlights -->
		<section>
			<ul class="divided">
				<li>

					<!-- Highlight -->
						<article class="box highlight">
							<?php dynamic_sidebar( 'highlight-sidebar' ); ?>
						</article>

				</li>
			</ul>
		</section>
		
</div>
