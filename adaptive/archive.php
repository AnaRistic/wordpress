<?php get_header(); ?>

<div class="container container-center">

	<div class="row">

		<div class="col-xs-12 col-sm-9">

			<?php 
    
    			if ( have_posts() ): ?>

                    <header class="page-header text-center">
                        <?php 

                            the_archive_title('<h1 class="page-title">', '</h1>');
                            the_archive_description('<div class="texonomy-description">', '</div>');

                        ?>
                    </header>

        			<?php while ( have_posts() ): the_post(); ?>

    					<?php get_template_part('content', 'archive'); ?>

        			<?php endwhile; ?>

                    <hr>

                    <footer class="enrty-footer">
                        <?php echo adaptive_post_navigation(); ?>
                    </footer>

        			<!--<div class="col-xs-12 text-center">
        				<?php //the_posts_navigation(); ?>
        			</div>-->

    			<?php endif;

			?>

		</div>

		<div class="col-xs-12 col-sm-3">
			<?php get_sidebar(); ?>
		</div>

	</div>

</div>

<?php get_footer(); ?>