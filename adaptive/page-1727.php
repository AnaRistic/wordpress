<?php get_header(); ?>

<div class="container">

	<div class="row">
			
		<?php

			$args_cat = array(
				'include' => '192, 193, 194',
			);

			$categories = get_categories($args_cat);

			foreach($categories as $category) :

				$args = array(
				'type' => 'post',
				'posts_per_page' => 1,
				'category__in' => $category->term_id,
				'caregory__not_in' => array(1)
				);

				$lastBlog = new WP_Query($args);

				if ( $lastBlog->have_posts() ):

        			while ( $lastBlog->have_posts() ): $lastBlog->the_post(); ?>

        				<div class="col-xs-12 col-sm-4">

    						<?php get_template_part('content', 'featured'); ?>

    					</div>

        			<?php endwhile;

    			endif;

    			wp_reset_postdata();

				endforeach;

		?>

	</div>

<div class="row">

		<div class="col-xs-12 col-sm-9">

		<?php 
    
    		if ( have_posts() ):

        		while ( have_posts() ): the_post(); ?>

            		<p><?php the_content(); ?></p>

            		<h3><?php the_title(); ?></h3>

            	<hr>

        		<?php endwhile;

    		endif;

    		//PRINT OTHER 2 POST NOT THE FIRS ONE

    		/*$args = array(
    			'type' => 'post',
    			'posts_per_page' => 2,
    			'offset' => 1
    		);

    		$lastBlog = new WP_Query($args);

			if ( $lastBlog->have_posts() ):

        		while ( $lastBlog->have_posts() ): $lastBlog->the_post(); ?>

    				<?php get_template_part('content', get_post_format()); ?>

        		<?php endwhile;

    		endif;

    		wp_reset_postdata();*/

			?>

			<!--<hr>-->

			<?php

			//PRINT ONLY TUTORIALS

    		/*$lastBlog = new WP_Query('type=post&posts_per_page=-1&category_name=news');

			if ( $lastBlog->have_posts() ):

        		while ( $lastBlog->have_posts() ): $lastBlog->the_post(); ?>

    				<?php get_template_part('content', get_post_format()); ?>

        		<?php endwhile;

    		endif;

    		wp_reset_postdata();*/

			?>

		</div>

		<div class="col-xs-12 col-sm-3">
			<?php get_sidebar(); ?>
		</div>

	</div>

</div>

<?php get_footer(); ?>