<?php

/*
	======================================
		ADMIN PAGE
	======================================
*/

function adaptive_add_admin_page() {

	//Generate Adaptive Admin Page
	add_menu_page( 'Adaptive', 'Adaptive', 'manage_options', 'anchy_adaptive', 'adaptive_theme_create_page', IMAGES . '/icons/sunset-icon.png', 110 );

	//Generate Adaptive Admin Sub Pages
	add_submenu_page('anchy_adaptive', 'Adaptive', 'General', 'manage_options', 'anchy_adaptive', 'adaptive_theme_create_page');

	add_submenu_page('anchy_adaptive', 'Adaptive Contact Form', 'Contact Form', 'manage_options', 'anchy_adaptive_theme_contact', 'adaptive_contact_form_page');

	//Activate custom settings
	add_action('admin_init', 'adaptive_custom_settings');


}

add_action('admin_menu', 'adaptive_add_admin_page');

function adaptive_custom_settings() {
	register_setting('adaptive-settings-group', 'first_name' );
	register_setting('adaptive-settings-group', 'last_name');

	add_settings_section('adaptive-sidebar-options', 'Sidebar Options', 'adaprive_sidebar_options', 'anchy_adaptive');

	add_settings_field('sidebar-name', 'Full Name', 'adaptive_sidebar_name', 'anchy_adaptive', 'adaptive-sidebar-options');

	//Contact Form Options
	register_setting( 'adaptive-contact-options', 'activate_contact' );

	add_settings_section('sunset-contact-section', 'Contact Form', 'adaptive_contact_section', 'anchy_adaptive_theme_contact');

	add_settings_field('activate-form', 'Activate Contact Form', 'adaptive_activate_contact', 'anchy_adaptive_theme_contact', 'sunset-contact-section');

}

function adaptive_contact_section() {
	echo "Activate and Deactivate the Built-in Contact Form";
}

function adaptive_activate_contact() {
	$options = get_option( 'activate_contact' );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="activate_contact" name="activate_contact" value="1" '.$checked.' /></label>';
}

function adaprive_sidebar_options() {
	echo "Customize your Sidebar Information";
}

function adaptive_sidebar_name() {
	$firstName = esc_attr( get_option( 'first_name' ) );
	$lastName = esc_attr( get_option( 'last_name' ) );
	echo '<input type="text" name="first_name" value="'.$firstName.'" placeholder="First Name"> <input type="text" name="last_name" value="'.$lastName.'" placeholder="Last Name">';
}

function adaptive_theme_create_page() {
	require_once( get_template_directory() . '/inc/templates/adaptive-admin.php' );
}

function adaptive_contact_form_page() {
	require_once( get_template_directory() . '/inc/templates/adaptive-contact-form.php' );
}