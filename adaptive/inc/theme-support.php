<?php

/******************************************************************************/
/* Theme support */
/******************************************************************************/
$defaults = array(
	'default-color' => '#fff',
);
add_theme_support('custom-background', $defaults);

$default = array(
	'default-text-color' => '#fff',
	'default-image' => IMAGES.'/header-background.jpg',
);
add_theme_support('custom-header',$default);

add_theme_support('custom-logo');

add_theme_support('post-thumbnails');
add_theme_support('post-formats', array('aside','image','video'));

/* Activate HTML5 features */
add_theme_support('html5', array( 'comment-list', 'comment-form', 'search-form' ) );

/******************************************************************************/
/* Sidebar function */
/******************************************************************************/
function adaptive_widget_setup() {

	register_sidebar(
		array(
			'name' => 'Sidebar',
			'id' => 'adaptive-sidebar',
			'class' => 'custom',
			'description' => 'Standard Sidebar',
			'before_widget' => '<aside id="%1$s" class="adaptive-widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h1 class="widget-title">',
			'after_title' => '</h1>',
		)
	);
}
add_action('widgets_init','adaptive_widget_setup');

/******************************************************************************/
/* Tags and Comments section of blog post */
/******************************************************************************/

function adaptive_posted_footer() {

	$comments_num = get_comments_number();
	if ( comments_open() ) {
		if ( $comments_num == 0 ) {
			$comments = __('No Comments', 'adaptive');
		} elseif ( $comments_num >1 ) {
			$comments = $comments_num . __(' Comments', 'adaptive');
		} else {
			$comments = __('1 Comment', 'adaptive');
		}
		$comments = '<a href="' . get_comments_link() . '">' . $comments . '</a><span class="glyphicon glyphicon-comment"></span>';
	} else {
		$comments = __('Comments are closed', 'adaptive');
	}

	return '<div class="post-footer-contaioner">
				<div class="row">
					<div class="col-xs-12 col-sm-8">
						' . get_the_tag_list('<span class="tags-list"><span class="glyphicon glyphicon-tags"></span>', ', ', '</span>') . '
					</div>
					<div class="col-xs-12 col-sm-4 text-right">
						' . $comments . '
					</div>
				</div>
			</div>';
}

/******************************************************************************/
/* SINGLE POST CUSTOM FUNCTION (NAVIGATIONS)*/
/******************************************************************************/

function adaptive_single_post_navigation() {

	$nav = '<div class="row">';

	$prev = get_previous_post_link( '<div class="post-link-nav">%link</div>', '<span class="glyphicon glyphicon-menu-left arroy" aria-hidden="true"></span> %title' );
	$nav .= '<div class="col-xs-12 col-sm-6 buttons">' . $prev . '</div>';

	$next = get_next_post_link( '<div class="post-link-nav">%link', '%title <span class="glyphicon glyphicon-menu-right arroy" aria-hidden="true"></span></div>' );
	$nav .= '<div class="col-xs-12 col-sm-6 text-right buttons">' . $next . '</div>';

	$nav .= '</div>';

	return $nav;

}

function adaptive_get_post_navigation() {
	//if( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ):

		require( GTD . '/inc/templates/adaptive-comment-nav.php' );

	//endif;
}

/******************************************************************************/
/* BLOG POST CUSTOM FUNCTION (NAVIGATIONS)*/
/******************************************************************************/

function adaptive_post_navigation() {
	
	$nav = '<div class="row">';

	$prev = get_next_posts_link('<span class="glyphicon glyphicon-menu-left arroy" aria-hidden="true"></span> Older Posts');

	$nav .= '<div class="col-xs-6 col-sm-6 text-left buttons"><div class="post-link-nav">' . $prev . '</div></div>';

	$next = get_previous_posts_link('Newer Posts <span class="glyphicon glyphicon-menu-right arroy" aria-hidden="true"></span>');

	$nav .= '<div class="col-xs-6 col-sm-6 text-right buttons"><div class="post-link-nav">' . $next . '</div></div>';

	$nav .= '</div>';

	return $nav;

}

/******************************************************************************/
/* Contact Form */
/******************************************************************************/

function mailtrap($phpmailer) {
  $phpmailer->isSMTP();
  $phpmailer->Host = 'smtp.mailtrap.io';
  $phpmailer->SMTPAuth = true;
  $phpmailer->Port = 2525;
  $phpmailer->Username = '030705b457c5b3';
  $phpmailer->Password = '0b2d4607414e95';
}

add_action('phpmailer_init', 'mailtrap');

/******************************************************************************/
/* BLOG POST CUSTOM FUNCTION (video) */
/******************************************************************************/

function adaptive_get_embedded_media( $type = array() ) {
	$content = do_shortcode( apply_filters( 'the_content', get_the_content() ) ); 
	$embed = get_media_embedded_in_content( $content, $type );

	if ( in_array( 'audio' , $type) ) {
		$output = str_replace( '?visual=true', '?visual=false', $embed[0] );
	} else {
		$output = $embed[0];
	}

	return $output;
}

