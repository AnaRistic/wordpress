<h1>Adaptive Custom CSS</h1>
<?php settings_errors(); ?>

<form method="post" action="options.php">
	<?php settings_fields('adaptive-custom-css-options'); ?>
	<?php do_settings_sections('anchy_adaptive_css'); ?>
	<?php submit_button(); ?>
</form>