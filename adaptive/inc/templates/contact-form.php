<div class="parallax-contact row" data-parallax="scroll" data-image-src="images/pimgpsh_fullsize_distr.jpg" data-z-index="1" data-speed="0.5">

        <div class="layer-contact"></div>

        <div class="contact col-sm-12">
            <h1>Contact Us</h1>

            <form class="sunsetContactForm" action="index.html" method="post">
                <span class="input input--hoshi">
                    <input class="input__field input__field--hoshi" type="text" id="email" name="email" />
                    <label class="input__label input__label--hoshi" for="email">
                        <span class="input__label-content input__label-content--hoshi">E-mail Address</span>
                    </label>
                </span>

                <span class="input input--hoshi">
                    <input class="input__field input__field--hoshi" type="text" id="text" name="text" />
                    <label class="input__label input__label--hoshi" for="text">
                        <span class="input__label-content input__label-content--hoshi">Write something to us</span>
                    </label>
                </span>

                <input type="submit" value="SEND" class="button-send">
            </form>
        </div>

    </div>