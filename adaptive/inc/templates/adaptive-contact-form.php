<h1>Adaptive Contact Form</h1>
<?php settings_errors(); ?>

<h4>Use this shortcode to activate the Contact Form inside a Page or a Post</h4>
<code>[contact]</code>

<form method="post" action="options.php">
	<?php settings_fields('adaptive-contact-options'); ?>
	<?php do_settings_sections('anchy_adaptive_theme_contact'); ?>
	<?php submit_button(); ?>
</form>