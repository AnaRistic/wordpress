<div class="parallax-contact row" data-parallax="scroll" data-image-src="images/pimgpsh_fullsize_distr.jpg" data-z-index="1" data-speed="0.5">

        <div class="layer-contact"></div>

        <div class="contact col-sm-12">
            <h1>Contact Us</h1>

            <form id="adaptiveContactForm" action="#" method="post" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
                <span class="input input--hoshi form-group">
                    <input class="input__field input__field--hoshi" type="text" id="email" name="email" />
                    <label class="input__label input__label--hoshi" for="email">
                        <span class="input__label-content input__label-content--hoshi">E-mail Address</span>
                    </label>
                    <h3 class="text_danger email-error">Your Email is Required</h3>
                </span>

                <span class="input input--hoshi form-group">
                    <input class="input__field input__field--hoshi" type="text" id="message" name="message" />
                    <label class="input__label input__label--hoshi" for="message">
                        <span class="input__label-content input__label-content--hoshi">Write something to us</span>
                    </label>
                    <h3 class="text_danger message-error">Your Message is Required</h3>
                </span>

                <button type="submit" class="button-send">SEND</button>
                <small class="text-info js-form-submission">Submission in proces, please wait...</small>
                <small class="text-success js-form-success">Message Successfully submitted, thank you!</small>
                <small class="text-danger js-form-error">There was a problem with the Contact Form, please try again!</small>
            </form>
        </div>

    </div>