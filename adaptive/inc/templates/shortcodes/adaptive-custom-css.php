<h1>Adaptive Custom CSS</h1>
<?php settings_errors(); ?>

<form method="post" action="options.php">
	<?php settings_fields('adaptive-contact-options'); ?>
	<?php do_settings_sections('anchy_adaptive_theme_contact'); ?>
	<?php submit_button(); ?>
</form>