<?php

/******************************************************************************/
/* Contact - Shortcode */
/******************************************************************************/

add_shortcode('contact', 'contact_simple_shortcode');

function contact_simple_shortcode($atts) {

    $atts = vc_map_get_attributes( 'contact', $atts );
    extract( $atts );

    $admin_url = admin_url("admin-ajax.php");

    $imgSrc = IMAGES.'/pimgpsh_fullsize_distr.jpg';

    (!empty($input_line)) ? $input_line_style = '.input__label--hoshi::after { border-bottom: 4px solid' . $input_line . '}' : $input_line_style = '';
    
    $layer_contact =
    (!empty($bg_color_1) && !empty($bg_color_2)) ? 
         ".layer-contact { 
            background: linear-gradient(to right, ".$bg_color_1.", ".$bg_color_2.");
            background: -webkit-linear-gradient( left, ".$bg_color_1.", ".$bg_color_2.");
            background: -o-linear-gradient( to right, ".$bg_color_1.", ".$bg_color_2.");
            background: -moz-linear-gradient( to right, ".$bg_color_1.", ".$bg_color_2.");" : '';


    return "
        <style>
            {$input_line_style}
            {$layer_contact}
        </style>
        <div class='parallax-contact row' data-parallax='scroll' data-image-src='{$imgSrc}' data-z-index='0' >

        <div class='layer-contact'></div>

        <div class='contact col-sm-12'>
            <h1>Contact Us</h1>

            <form id='adaptiveContactForm' action='#' method='post' data-url='{$admin_url}'>
                <span class='input input--hoshi form-group'>
                    <input class='input__field input__field--hoshi' type='text' id='email' name='email' required/>
                    <label class='input__label input__label--hoshi' for='email'>
                        <span class='input__label-content input__label-content--hoshi'>E-mail Address</span>
                    </label>
                    <h3 class='text_danger email-error'>Your Email is Required</h3>
                </span>

                <span class='input input--hoshi form-group'>
                    <input class='input__field input__field--hoshi' type='text' id='message' name='message' required/>
                    <label class='input__label input__label--hoshi' for='message'>
                        <span class='input__label-content input__label-content--hoshi'>Write something to us</span>
                    </label>
                    <h3 class='text_danger message-error'>Your Message is Required</h3>
                </span>

                <button type='submit' class='button-send' style='background-color:{$background_button_color}; color:{$button_text_color};'>SEND</button>
                <small class='text-info js-form-submission'>Submission in proces, please wait...</small>
                <small class='text-success js-form-success'>Message Successfully submitted, thank you!</small>
                <small class='text-danger js-form-error'>There was a problem with the Contact Form, please try again!</small>
            </form>
        </div>

    </div>

    ";

}

add_action( 'vc_before_init', 'contact_form_function' );
function contact_form_function() {
   vc_map( array(
      "name" => __( "Contact", "adaptive" ),
      "base" => "contact",
      "class" => "parallax-contact",
      "category" => __( "Content", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Linear gradient", "adaptive" ),
            "param_name" => "bg_color_1",
            "value" => '#ca1f5a',
        ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "param_name" => "bg_color_2",
            "value" => '#732b84', 
            "description" => __( "Choose background colors...", "adaptive" )
        ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Button background color", "adaptive" ),
            "param_name" => "background_button_color",
            "value" => '#fff', 
            "description" => __( "Choose button background color...", "adaptive" )
        ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Button text color", "adaptive" ),
            "param_name" => "button_text_color",
            "value" => '#ca1f5a', 
            "description" => __( "Choose button text color...", "adaptive" )
        ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Color of input line", "adaptive" ),
            "param_name" => "input_line",
            "value" => '#cc004e', 
            "description" => __( "Choose color of input line...", "adaptive" )
        ),
      )
   ) );
}