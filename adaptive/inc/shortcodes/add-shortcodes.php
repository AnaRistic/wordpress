<?php

/******************************************************************************/
/* Shortcodes */
/******************************************************************************/

// OurServices - shortcode

add_shortcode('our_services', 'our_services_simple_shortcode');
function our_services_simple_shortcode($atts) {

    extract( shortcode_atts( array(
        'main_title' => 'Our Services',
        'title1' => 'Web Design',
        'title2' => 'Development',
        'title3' => 'Video Editing',
        'contant1' => 'Lorem ipsum dolor sit amet, consectetur adipisacing elit. Vivamus congue euerat at mattis.',
        'contant2' => 'Lorem ipsum dolor sit amet, consectetur adipisacing elit. Vivamus congue euerat at mattis.',
        'contant3' => 'Lorem ipsum dolor sit amet, consectetur adipisacing elit. Vivamus congue euerat at mattis.',

   ), $atts ) );

    $a1 = shortcode_atts(array(
            'image_url1' => 'image_url1',
        ), $atts);

        $img1 = wp_get_attachment_image_src($a1["image_url1"], "thumbnail");

        if ($img1) {
            $imgSrc1 = $img1[0];
        } else {
            $imgSrc1 = IMAGES.'/tap.svg';
        }

    $a2 = shortcode_atts(array(
            'image_url2' => 'image_url2',
        ), $atts);

        $img2 = wp_get_attachment_image_src($a2["image_url2"], "thumbnail");

        if ($img2) {
            $imgSrc2 = $img2[0];
        } else {
            $imgSrc2 = IMAGES.'/settings.svg';
        }

    $a3 = shortcode_atts(array(
            'image_url3' => 'image_url3',
        ), $atts);

        $img3 = wp_get_attachment_image_src($a3["image_url3"], "thumbnail");

        if ($img3) {
            $imgSrc3 = $img3[0];
        } else {
            $imgSrc3 = IMAGES.'/support.svg';
        }




    return "<div class='service-header'><span>{$main_title}</span></div>

            <div class='row text-center' style='width: 80%; margin: auto;''>
        <div class='col-md-4'>
            <div class='thumbnail'>
                <a href=''>
                    <div>
                        <img src='{$imgSrc1}' class='icon'>
                    </div>
                    <h3>{$title1}</h3>
                    <p>{$contant1}</p>
                </a>
            </div>
        </div>

        <div class='col-md-4'>
            <div class='thumbnail'>
                <a href=''>
                    <div>
                        <img src='{$imgSrc2}' class='icon'>
                    </div>
                    <h3>{$title2}</h3>
                    <p>{$contant2}</p>
                </a>
            </div>
        </div>
        
        <div class='col-md-4'>
            <div class='thumbnail'>
                <a href=''>
                    <div>
                        <img src='{$imgSrc3}' class='icon'>
                    </div>
                    <h3>{$title3}</h3>
                    <p>{$contant3}</p>
                </a>
            </div>
        </div>
    </div>
    ";
}

add_action( 'vc_before_init', 'our_services_function' );
function our_services_function() {
   vc_map( array(
      "name" => __( "Our Services", "adaptive" ),
      "base" => "our_services",
      "class" => "",
      "category" => __( "Content", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Main Title", "adaptive" ),
            "param_name" => "main_title",
            "value" => __( "Our Services", "adaptive" ),
            "description" => __( "Enter main title name.", "adaptive" )
         ),
         array(
            "type" => "textfield",
            "holder" => "h3",
            "class" => "",
            "heading" => __( "First Title", "adaptive" ),
            "param_name" => "title1",
            "value" => __( "Web Design", "adaptive" ),
            "description" => __( "Enter first title name.", "adaptive" )
         ),
         array(
            "type" => "textfield",
            "holder" => "h3",
            "class" => "",
            "heading" => __( "Second Title", "adaptive" ),
            "param_name" => "title2",
            "value" => __( "Development", "adaptive" ),
            "description" => __( "Enter second title name.", "adaptive" )
         ),
         array(
            "type" => "textfield",
            "holder" => "h3",
            "class" => "",
            "heading" => __( "Third Title", "adaptive" ),
            "param_name" => "title3",
            "value" => __( "Video Editing", "adaptive" ),
            "description" => __( "Enter third title name.", "adaptive" )
         ),
         array(
            "type" => "textarea",
            "holder" => "div",
            "class" => "",
            "heading" => __( "First Content", "adaptive" ),
            "param_name" => "contant1",
            "value" => __( "Lorem ipsum dolor sit amet, consectetur adipisacing elit. Vivamus congue euerat at mattis.", "adaptive" ),
            "description" => __( "Enter text.", "adaptive" )
         ),
         array(
            "type" => "textarea",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Second Content", "adaptive" ),
            "param_name" => "contant2",
            "value" => __( "Lorem ipsum dolor sit amet, consectetur adipisacing elit. Vivamus congue euerat at mattis.", "adaptive" ),
            "description" => __( "Enter text.", "adaptive" )
         ),
         array(
            "type" => "textarea",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Third Content", "adaptive" ),
            "param_name" => "contant3",
            "value" => __( "Lorem ipsum dolor sit amet, consectetur adipisacing elit. Vivamus congue euerat at mattis.", "adaptive" ),
            "description" => __( "Enter text.", "adaptive" )
         ),
         array(
                "type" => "attach_image",
                "heading" => __("First Image", "js_composer"),
                "holder" => "img",
                "class" => "",
                "param_name" => "image_url1",
                "description" => __("Choose image...", "js_composer")
            ),
         array(
                "type" => "attach_image",
                "heading" => __("Second Image", "js_composer"),
                "holder" => "img",
                "class" => "",
                "param_name" => "image_url2",
                "description" => __("Choose image...", "js_composer")
            ),
         array(
                "type" => "attach_image",
                "heading" => __("Third Image", "js_composer"),
                "holder" => "img",
                "class" => "",
                "param_name" => "image_url3",
                "description" => __("Choose image...", "js_composer")
            ),
      )
   ) );
}


// featuredProject - shortcode

add_shortcode( 'featured_project', 'featured_project_simple_shortcode' );
function featured_project_simple_shortcode( $atts, $content ) { 

   extract( shortcode_atts( array(
      'title' => 'Onion Website Template for Wordpress',
      'button_color' => '#ca1f5a',
      'content' => !empty($content) ? $content : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis dui sodales, vuliputate odi vitae, scelerisque dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis dui sodales, vuliputate odi vitae, scelerisque dolor.',

   ), $atts ) );

    $a = shortcode_atts(array(
            'image_url' => 'image_url',
        ), $atts);

        $img = wp_get_attachment_image_src($a["image_url"], "large");

        if ($img) {
            $imgSrc = $img[0];
        } else {
            $imgSrc = IMAGES.'/pimgpsh_fullsize_distr.png';
        }

  
   return "<div class='row featuredProject'>

        <div class='col-md-5 col-sm-12 col-xs-12 headerFP'>
            <h4>FEATURED PROJECT</h4>
            <h1>{$title}</h1>
            <p>{$content}</p>
            <a href='' class='btn contactButton margin0' style='background-color:{$button_color}'>VIEW PROJECT</a>
        </div>
        <div class='col-md-7 vertical'>
            <img class='img-responsive' src='{$imgSrc}'>
        </div>

    </div>";
}

add_action( 'vc_before_init', 'featured_project_function' );
function featured_project_function() {
   vc_map( array(
      "name" => __( "Featured Project", "adaptive" ),
      "base" => "featured_project",
      "class" => "featuredProject",
      "category" => __( "Content", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Title", "adaptive" ),
            "param_name" => "title",
            "value" => __( "Onion Website Template for Wordpress", "adaptive" ),
            "description" => __( "Enter title name.", "adaptive" )
         ),
         array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Button background color", "adaptive" ),
            "param_name" => "button_color",
            "value" => '#ca1f5a',
            "description" => __( "Choose background button color", "adaptive" )
         ),
         array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Content", "adaptive" ),
            "param_name" => "content",
            "value" => __( "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis dui sodales, vuliputate odi vitae, scelerisque dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis dui sodales, vuliputate odi vitae, scelerisque dolor.", "adaptive" ),
            "description" => __( "Enter content.", "adaptive" )
         ),
         array(
                "type" => "attach_image",
                "heading" => __("Image", "js_composer"),
                "holder" => "div",
                "class" => "",
                "param_name" => "image_url",
                "description" => __("Your desc", "js_composer")
            ),
      )
   ) );
}



// OurTeam - shortcode

add_shortcode('our_team', 'our_team_simple_shortcode');
function our_team_simple_shortcode($atts) {

    $team = vc_param_group_parse_atts( $atts['team'] );

    extract( shortcode_atts( array(
        'main_title' => 'Our Team',
        'select_class' => '',

   ), $atts ) );

    $a1 = shortcode_atts(array(
            'image_url1' => 'image_url1',
        ), $atts);

        $img1 = wp_get_attachment_image_src($a1["image_url1"], "small");

        if ($img1) {
            $imgSrc1 = $img1[0];
        } else {
            $imgSrc1 = IMAGES.'/SquareTeam5.jpg';
        }

    $a2 = shortcode_atts(array(
            'image_url2' => 'image_url2',
        ), $atts);

        $img2 = wp_get_attachment_image_src($a2["image_url2"], "small");

        if ($img2) {
            $imgSrc2 = $img2[0];
        } else {
            $imgSrc2 = IMAGES.'/SquareTeam4.jpg';
        }

    $a3 = shortcode_atts(array(
            'image_url3' => 'image_url3',
        ), $atts);

        $img3 = wp_get_attachment_image_src($a3["image_url3"], "small");

        if ($img3) {
            $imgSrc3 = $img3[0];
        } else {
            $imgSrc3 = IMAGES.'/SquareTeam3.jpg';
        }

    $a4 = shortcode_atts(array(
            'image_url4' => 'image_url4',
        ), $atts);

        $img4 = wp_get_attachment_image_src($a4["image_url4"], "small");

        if ($img4) {
            $imgSrc4 = $img4[0];
        } else {
            $imgSrc4 = IMAGES.'/SquareTeam2.jpg';
        }

    $a5 = shortcode_atts(array(
            'image_url5' => 'image_url5',
        ), $atts);

        $img5 = wp_get_attachment_image_src($a5["image_url5"], "small");

        if ($img5) {
            $imgSrc5 = $img5[0];
        } else {
            $imgSrc5 = IMAGES.'/SquareTeam1.jpg';
        }

    $a6 = shortcode_atts(array(
            'image_url6' => 'image_url6',
        ), $atts);

        $img6 = wp_get_attachment_image_src($a6["image_url6"], "small");

        if ($img6) {
           $imgSrc6 = $img6[0];
        } else {
            $imgSrc6 = IMAGES.'/SquareTeam8.jpg';
        }

    $a7 = shortcode_atts(array(
            'image_url7' => 'image_url7',
        ), $atts);

        $img7 = wp_get_attachment_image_src($a7["image_url7"], "small");

        if ($img7) {
           $imgSrc7 = $img7[0];
        } else {
            $imgSrc7 = IMAGES.'/SquareTeam7.jpg';
        }

    $a8 = shortcode_atts(array(
            'image_url8' => 'image_url8',
        ), $atts);

        $img8 = wp_get_attachment_image_src($a8["image_url8"], "small");

        if ($img8) {
           $imgSrc8 = $img8[0];
        } else {
            $imgSrc8 = IMAGES.'/SquareTeam6.jpg';
        }


    switch ($select_class) {
        case 'six':
            $class_six = 'col-md-4 col-sm-6 display-block';
            break;
        case 'eight':
            $class_eight = 'col-md-3 col-sm-6 display-block';
            break;
        case 'twelve':
            $class_twelve = 'col-md-2 col-sm-4 display-block';
            break;
        
        default:
            $class_eight = 'col-md-3 col-sm-6 display-block';
            break;
    }
   

	return "<div class='service-header'><span>{$main_title}</span></div>

        <div class='row team-row'>
            	<div class='{$class_six} {$class_eight} {$class_twelve} team display-none'>
                	<img src='{$imgSrc1}' class='img-responsive teamImg'>
                    	<h4 class='text'>HUMAN RESOURCES</h4>
                	<div class='teamDiv'>
                    	<h2>{$team[0]['title']}</h2>
                    	<p>{$team[0]['content']}</p>
                	</div>
            	</div>
            	<div class='{$class_six} {$class_eight} {$class_twelve} team display-none'>
            	    <img src='{$imgSrc2}' class='img-responsive teamImg'>
            	        <h4 class='text'>HUMAN RESOURCES</h4>
            	    <div class='teamDiv'>
            	        <h2>{$team[1]['title']}</h2>
            	        <p>{$team[1]['content']}</p>
                	</div>
            	</div>
            	<div class='{$class_six} {$class_eight} {$class_twelve} team display-none'>
            	    <img src='{$imgSrc3}' class='img-responsive teamImg'>
            	        <h4 class='text'>HUMAN RESOURCES</h4>
            	    <div class='teamDiv'>
            	        <h2>{$team[2]['title']}</h2>
            	        <p>{$team[2]['content']}</p>
                	</div>
            	</div>
            	<div class='{$class_six} {$class_eight} {$class_twelve} team display-none'>
            	    <img src='{$imgSrc4}' class='img-responsive teamImg'>
            	        <h4 class='text'>HUMAN RESOURCES</h4>
            	    <div class='teamDiv'>
            	        <h2>{$team[3]['title']}</h2>
            	        <p>{$team[3]['content']}</p>
                	</div>
            	</div>
    	        <div class='{$class_six} {$class_eight} {$class_twelve} team display-none'>
    	            <img src='{$imgSrc5}' class='img-responsive teamImg'>
    	               <h4 class='text'>HUMAN RESOURCES</h4>
    	            <div class='teamDiv'>
    	               <h2>{$team[4]['title']}</h2>
    	               <p>{$team[4]['content']}</p>
                	</div>
            	</div>
            	<div class='{$class_six} {$class_eight} {$class_twelve} team display-none'>
            	    <img src='{$imgSrc6}' class='img-responsive teamImg'>
            	        <h4 class='text'>HUMAN RESOURCES</h4>
            	    <div class='teamDiv'>
            	        <h2>{$team[5]['title']}</h2>
            	        <p>{$team[5]['content']}</p>
                	</div>
            	</div>
            	<div class='{$class_eight} {$class_twelve} team display-none'>
            	    <img src='{$imgSrc7}' class='img-responsive teamImg'>
            	        <h4 class='text'>HUMAN RESOURCES</h4>
            	    <div class='teamDiv'>
            	        <h2>{$team[6]['title']}</h2>
            	        <p>{$team[6]['content']}</p>
               		</div>
            	</div>
            	<div class='{$class_eight} {$class_twelve} team display-none'>
            	    <img src='{$imgSrc8}' class='img-responsive teamImg'>
            	        <h4 class='text'>HUMAN RESOURCES</h4>
            	    <div class='teamDiv'>
            	        <h2>{$team[7]['title']}</h2>
            	        <p>{$team[7]['content']}</p>
                	</div>
            	</div>
                <div class='{$class_twelve} team display-none'>
                    <img src='{$imgSrc1}' class='img-responsive teamImg'>
                        <h4 class='text'>HUMAN RESOURCES</h4>
                    <div class='teamDiv'>
                        <h2>{$team[8]['title']}</h2>
                        <p>{$team[8]['content']}</p>
                    </div>
                </div>
                <div class='{$class_twelve} team display-none'>
                    <img src='{$imgSrc2}' class='img-responsive teamImg'>
                        <h4 class='text'>HUMAN RESOURCES</h4>
                    <div class='teamDiv'>
                        <h2>{$team[9]['title']}</h2>
                        <p>{$team[9]['content']}</p>
                    </div>
                </div>
                <div class='{$class_twelve} team display-none'>
                    <img src='{$imgSrc3}' class='img-responsive teamImg'>
                        <h4 class='text'>HUMAN RESOURCES</h4>
                    <div class='teamDiv'>
                        <h2>{$team[10]['title']}</h2>
                        <p>{$team[10]['content']}</p>
                    </div>
                </div>
                <div class='{$class_twelve} team display-none'>
                    <img src='{$imgSrc4}' class='img-responsive teamImg'>
                        <h4 class='text'>HUMAN RESOURCES</h4>
                    <div class='teamDiv'>
                        <h2>{$team[11]['title']}</h2>
                        <p>{$team[11]['content']}</p>
                    </div>
                </div>
        </div>";
}

add_action( 'vc_before_init', 'our_team_function' );
function our_team_function() {
   vc_map( array(
      "name" => __( "Our Team", "adaptive" ),
      "base" => "our_team",
      "class" => "",
      "category" => __( "Content", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "service-header",
            "heading" => __( "Main Title", "adaptive" ),
            "param_name" => "main_title",
            "value" => __( "Our Team", "adaptive" ),
            "description" => __( "Enter main title name.", "adaptive" )
         ),
         array(
            "type"        => "checkbox",
            "heading"     => __("Select how many team members do you want", "adaptive" ),
            "param_name"  => "select_class",
            "admin_label" => true,
            "value"       => array(
                                'Six members' => 'six',
                                'Eight members' => 'eight',
                                'Twelve members' => 'twelve',
                                ), //value
            "std"         => " ",
            "description" => __("Select just one option.", "adaptive" )
         ),
         // params group
            array(
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'team',
                // Note params is mapped inside param-group:
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => 'Michael Statham',
                        'heading' => __('Enter your title', 'adaptive'),
                        'param_name' => 'title',
                    ),
                    array(
                        'type' => 'textarea',
                        'value' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis dui sodales, vuliputate odi vitae, scelerisque dolor.', 'adaptive' ),
                        'heading' => __('Enter your content', 'adaptive'),
                        'param_name' => 'content',
                    ),
                    /*array(
                        "type" => "attach_image",
                        "heading" => __("Select Image", "js_composer"),
                        "holder" => "img",
                        "class" => "teamImg",
                        "param_name" => "image_url",
                        "description" => __("Choose image...", "js_composer")
                    ),*/
                )
            ),
         array(
                "type" => "attach_image",
                "heading" => __("First Image", "js_composer"),
                "holder" => "img",
                "class" => "teamImg",
                "param_name" => "image_url1",
                "description" => __("Choose image...", "js_composer")
            ),
        /* array(
                "type" => "attach_image",
                "heading" => __("Second Image", "js_composer"),
                "holder" => "img",
                "class" => "teamImg",
                "param_name" => "image_url2",
                "description" => __("Choose image...", "js_composer")
            ),
         array(
                "type" => "attach_image",
                "heading" => __("Third Image", "js_composer"),
                "holder" => "img",
                "class" => "teamImg",
                "param_name" => "image_url3",
                "description" => __("Choose image...", "js_composer")
            ),
         array(
                "type" => "attach_image",
                "heading" => __("Forth Image", "js_composer"),
                "holder" => "img",
                "class" => "teamImg",
                "param_name" => "image_url4",
                "description" => __("Choose image...", "js_composer")
            ),
         array(
                "type" => "attach_image",
                "heading" => __("Fifth Image", "js_composer"),
                "holder" => "img",
                "class" => "teamImg",
                "param_name" => "image_url5",
                "description" => __("Choose image...", "js_composer")
            ),
         array(
                "type" => "attach_image",
                "heading" => __("Sixth Image", "js_composer"),
                "holder" => "img",
                "class" => "teamImg",
                "param_name" => "image_url6",
                "description" => __("Choose image...", "js_composer")
            ),
         array(
                "type" => "attach_image",
                "heading" => __("Seventh Image", "js_composer"),
                "holder" => "img",
                "class" => "teamImg",
                "param_name" => "image_url7",
                "description" => __("Choose image...", "js_composer")
            ),
         array(
                "type" => "attach_image",
                "heading" => __("Eighth Image", "js_composer"),
                "holder" => "img",
                "class" => "teamImg",
                "param_name" => "image_url8",
                "description" => __("Choose image...", "js_composer")
            ),*/
      )
   ) );
}



// Purchase - shortcode

add_shortcode( 'purchase', 'purchase_simple_shortcode' );
function purchase_simple_shortcode( $atts, $content ) { 

   extract( shortcode_atts( array(
      'title' => 'Content Timeline for Wordpress',
      'button_color' => '#ca1f5a',
      'content' => !empty($content) ? $content : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis dui sodales, vuliputate odi vitae, scelerisque dolor. Lorem ipsum dolor sit amet.',

   ), $atts ) );

    $a = shortcode_atts(array(
            'image_url' => 'image_url',
        ), $atts);

        $img = wp_get_attachment_image_src($a["image_url"], "large");

        if ($img) {
           $imgSrc = $img[0];
        } else {
            $imgSrc = IMAGES.'/purchase-background.jpg';
        }
  
   return "<div class='row purchase' style='background: url({$imgSrc});'>
        <div class='col-md-12 purchaseDiv headerFP'>
            <h1>{$title}</h1>
            <p>{$content}</p>
            <a href='' class='btn contactButton margin0' style='background-color:{$button_color}'>VIEW PROJECT</a>
        </div>
    </div>";
}

add_action( 'vc_before_init', 'purchase_function' );
function purchase_function() {
   vc_map( array(
      "name" => __( "Purchase", "adaptive" ),
      "base" => "purchase",
      "class" => "purchase",
      "category" => __( "Content", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Title", "adaptive" ),
            "param_name" => "title",
            "value" => __( "Content Timeline for Wordpress", "adaptive" ),
            "description" => __( "Enter title name.", "adaptive" )
         ),
         array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Button background color", "adaptive" ),
            "param_name" => "button_color",
            "value" => '#ca1f5a',
            "description" => __( "Choose background button color", "adaptive" )
         ),
         array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Content", "adaptive" ),
            "param_name" => "content",
            "value" => __( "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis dui sodales, vuliputate odi vitae, scelerisque dolor. Lorem ipsum dolor sit amet.", "adaptive" ),
            "description" => __( "Enter content.", "adaptive" )
         ),
         array(
                "type" => "attach_image",
                "heading" => __("Image", "js_composer"),
                "holder" => "div",
                "class" => "",
                "param_name" => "image_url",
                "description" => __("Your desc", "js_composer")
            ),
      )
   ) );
}



// Contact - shortcode


add_shortcode('contact', 'contact_simple_shortcode');

function contact_simple_shortcode($atts) {

    extract( shortcode_atts( array(
      'background' => '',
      'background_button_color' => '#fff',
      'button_text_color' => '#ca1f5a',
      'selectclassname' => '',
   ), $atts ) );

    $admin_url = admin_url("admin-ajax.php");

    $imgSrc = IMAGES.'/pimgpsh_fullsize_distr.jpg';


    return "<div class='parallax-contact row' data-parallax='scroll' data-image-src='{$imgSrc}' data-z-index='0' >

        <div class='layer-contact' style='background:{$background};'></div>

        <div class='contact col-sm-12'>
            <h1>Contact Us</h1>

            <form id='adaptiveContactForm' action='#' method='post' data-url='{$admin_url}'>
                <span class='input input--hoshi form-group'>
                    <input class='input__field input__field--hoshi' type='text' id='email' name='email' />
                    <label class='input__label input__label--hoshi {$selectclassname}' for='email'>
                        <span class='input__label-content input__label-content--hoshi'>E-mail Address</span>
                    </label>
                    <h3 class='text_danger email-error'>Your Email is Required</h3>
                </span>

                <span class='input input--hoshi form-group'>
                    <input class='input__field input__field--hoshi' type='text' id='message' name='message' />
                    <label class='input__label input__label--hoshi {$selectclassname}' for='message'>
                        <span class='input__label-content input__label-content--hoshi'>Write something to us</span>
                    </label>
                    <h3 class='text_danger message-error'>Your Message is Required</h3>
                </span>

                <button type='submit' class='button-send' style='background-color:{$background_button_color}; color:{$button_text_color};'>SEND</button>
                <small class='text-info js-form-submission'>Submission in proces, please wait...</small>
                <small class='text-success js-form-success'>Message Successfully submitted, thank you!</small>
                <small class='text-danger js-form-error'>There was a problem with the Contact Form, please try again!</small>
            </form>
        </div>

    </div>

    ";

}

add_action( 'vc_before_init', 'contact_form_function' );
function contact_form_function() {
   vc_map( array(
      "name" => __( "Contact", "adaptive" ),
      "base" => "contact",
      "class" => "parallax-contact",
      "category" => __( "Content", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Background color", "adaptive" ),
            "param_name" => "background",
            "value" => '#ca1f5a', 
            "description" => __( "Choose background color", "adaptive" )
        ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Background button color", "adaptive" ),
            "param_name" => "background_button_color",
            "value" => '#fff', 
            "description" => __( "Choose button background color", "adaptive" )
        ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Button text color", "adaptive" ),
            "param_name" => "button_text_color",
            "value" => '#ca1f5a', 
            "description" => __( "Choose button text color", "adaptive" )
        ),
        array(
            "type"        => "checkbox",
            "heading"     => __("Select just one option"),
            "param_name"  => "selectclassname",
            "admin_label" => true,
            "value"       => array(
                                'First Option'=>'input__label--hoshi-color-1',
                                'Second Option'=>'input__label--hoshi-color-2',
                                'Third Option'=>'input__label--hoshi-color-3',
                                ), //value
            "std"         => " ",
            "description" => __("Select one of three options for changing color of form inputs.")
        ),
      )
   ) );
}



// Social Icons - shortcodes


/*add_shortcode('socialrow', 'social_icons_row_shortcode');
function social_icons_row_shortcode( $atts, $content ) {
    return "<div class='row social'>" . do_shortcode($content) . "</div>";
}*/


//tumblr - shortcode
add_shortcode('tumblr','tumblr_simple_shortcode');
function tumblr_simple_shortcode($atts) {

    extract( shortcode_atts( array(
      'link' => '',
      'change_hover_color' => ''
   ), $atts ) );

	 return "<div class='col-md-2 col-sm-4 col-xs-4'>
                <a href='https://www.tumblr.com/{$link}'>
                    <?xml version='1.0' encoding='iso-8859-1'?>
                    <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                    <!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>
                    <svg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'
                        width='430.118px' height='430.118px' viewBox='0 0 430.118 430.118' style='enable-background:new 0 0 430.118 430.118;' 
                        xml:space='preserve' class='social-icons {$change_hover_color}'>
                    <g>
                        <path id='Tumblr' d='M252.797,351.543c-7.229-4.247-13.866-11.547-16.513-18.589c-2.679-7.09-2.338-21.455-2.338-46.419V176.243
                        h100.301V99.477H233.951V0h-61.713c-2.753,22.155-7.824,40.459-15.18,54.815c-7.369,14.377-17.088,26.658-29.276,36.924
                        c-12.127,10.246-31.895,18.143-48.927,23.589v60.915h58.922v150.836c0,19.694,2.088,34.718,6.24,45.061
                        c4.172,10.333,11.623,20.124,22.386,29.337c10.762,9.115,23.758,16.228,39.003,21.226c15.227,4.942,26.936,7.416,46.767,7.416
                        c17.445,0,33.687-1.759,48.747-5.198c15.042-3.529,31.834-9.605,50.344-18.221v-67.859c-21.721,14.169-43.567,21.221-65.535,21.221
                        C273.364,360.065,262.435,357.223,252.797,351.543z'/>
                    </g>
                </a>
            </div>";
}

add_action( 'vc_before_init', 'tumblr_function' );
function tumblr_function() {
   vc_map( array(
      "name" => __( "Tumblr", "adaptive" ),
      "base" => "tumblr",
      "class" => "",
      "category" => __( "Social", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
            array(
            "type" => "textfield",
            "holder" => "a",
            "class" => "",
            "heading" => __( "Username", "adaptive" ),
            "param_name" => "link",
            "value" => "",
            "description" => __( "Enter your username. All lowercase letters and no spaces.", "adaptive" )
         ),
            array(
            "type" => "textfield",
            "holder" => "a",
            "class" => "",
            "heading" => __( "Change hover color", "adaptive" ),
            "param_name" => "change_hover_color",
            "value" => "",
            "description" => __( "To change hover color of button enter 'iconhoverblue' for blue color, or 'iconhovergreen' for green color.", "adaptive" )
         ),
      )
   ) );
}

//vimeo - Shordcode
add_shortcode('vimeo','vimeo_simple_shortcode');
function vimeo_simple_shortcode($atts) {

	extract( shortcode_atts( array(
      'link' => '',
      'change_hover_color' => ''
   ), $atts ) );

    return "<div class='col-md-2 col-sm-4 col-xs-4'>
            <a href='https://vimeo.com/{$link}'>
                <?xml version='1.0' encoding='iso-8859-1'?>
                <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                <!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>
                <svg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' width='430.118px' height='430.118px' viewBox='0 0 430.118 430.118' style='enable-background:new 0 0 430.118 430.118;'' xml:space='preserve' class='social-icons {$change_hover_color}'>
                <g>
                    <path id='Vimeo' d='M367.243,28.754c-59.795-1.951-100.259,31.591-121.447,100.664c10.912-4.494,21.516-6.762,31.858-6.762 c21.804,0,31.455,12.237,28.879,36.776c-1.278,14.86-10.911,36.482-28.879,64.858c-18.039,28.423-31.513,42.61-40.464,42.61 c-11.621,0-22.199-21.958-31.857-65.82c-3.239-12.918-9.031-45.812-17.324-98.765c-7.775-49.046-28.32-71.962-61.727-68.741 C112.15,34.873,90.98,47.815,62.726,72.308C42.113,91.032,21.228,109.761,0,128.471l20.225,26.112 c19.303-13.562,30.595-20.311,33.731-20.311c14.802,0,28.625,23.219,41.488,69.651c11.53,42.644,23.158,85.23,34.744,127.812 c17.256,46.466,38.529,69.708,63.552,69.708c40.473,0,90.028-38.065,148.469-114.223c56.537-72.909,85.725-130.352,87.694-172.341 C432.498,58.764,411.613,30.028,367.243,28.754z'/>
                </g>
                </svg>
            </a>
            </div>";

}

add_action( 'vc_before_init', 'vimeo_function' );
function vimeo_function() {
   vc_map( array(
      "name" => __( "Vimeo", "adaptive" ),
      "base" => "vimeo",
      "class" => "",
      "category" => __( "Social", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
            array(
            "type" => "textfield",
            "holder" => "a",
            "class" => "",
            "heading" => __( "Username", "adaptive" ),
            "param_name" => "link",
            "value" => "",
            "description" => __( "Enter your username. All lowercase letters and no spaces.", "adaptive" )
         ),
            array(
            "type" => "textfield",
            "holder" => "a",
            "class" => "",
            "heading" => __( "Change hover color", "adaptive" ),
            "param_name" => "change_hover_color",
            "value" => "",
            "description" => __( "To change hover color of button enter 'iconhoverblue' for blue color, or 'iconhovergreen' for green color.", "adaptive" )
         ),
      )
   ) );
}


//digg - shortcode
add_shortcode('digg','digg_simple_shortcode');
function digg_simple_shortcode($atts) {

	extract( shortcode_atts( array(
      'link' => '',
      'change_hover_color' => ''
   ), $atts ) );

    return "<div class='col-md-2 col-sm-4 col-xs-4'>
            <a href='http://digg.com/{$link}''>
                <?xml version='1.0' encoding='iso-8859-1'?>
                <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                <!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>
                <svg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'
                     width='430.117px' height='430.117px' viewBox='0 0 430.117 430.117' style='enable-background:new 0 0 430.117 430.117;'
                     xml:space='preserve' class='social-icons {$change_hover_color}'>
                <g>
                    <path id='Digg' d='M76.465,62.128v76.465H0v3.25v36.058v73.504l0,0v40.119l0,0l0,0h44.794l0,0h30.29l0,0h44.393V178.714v-42.41
                        V62.128H76.465z M43.012,253.291v-76.465h33.454v76.465H43.012z M129.035,138.593h43.012v152.931h-43.012V138.593z M129.035,62.128
                        h43.012v43.012h-43.012V62.128z M305.861,138.593L305.861,138.593H191.163v40.121l0,0v112.811h71.687v33.453h-71.687v43.012h71.72
                        h42.979v-74.678v-43.297V138.593L305.861,138.593z M234.175,253.291v-76.465h28.675v76.465H234.175z M430.117,138.593H315.42l0,0
                        l0,0v112.812v40.119h76.465v33.453H315.42v43.012h75.242h39.455v-45.85v-28.329v-42.405v-72.691l0,0V138.593z M391.885,253.291
                        h-33.453v-76.465h33.453V253.291z'/>
                </g>
                </svg>
            </a>
            </div>";
}

add_action( 'vc_before_init', 'digg_function' );
function digg_function() {
   vc_map( array(
      "name" => __( "Digg", "adaptive" ),
      "base" => "digg",
      "class" => "",
      "category" => __( "Social", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
            array(
            "type" => "textfield",
            "holder" => "a",
            "class" => "",
            "heading" => __( "Username", "adaptive" ),
            "param_name" => "link",
            "value" => "",
            "description" => __( "Enter your username. All lowercase letters and no spaces.", "adaptive" )
         ),
            array(
            "type" => "textfield",
            "holder" => "a",
            "class" => "",
            "heading" => __( "Change hover color", "adaptive" ),
            "param_name" => "change_hover_color",
            "value" => "",
            "description" => __( "To change hover color of button enter 'iconhoverblue' for blue color, or 'iconhovergreen' for green color.", "adaptive" )
         ),
      )
   ) );
}


//behance - shortcode
add_shortcode('behance','behance_simple_shortcode');
function behance_simple_shortcode($atts) {

	extract( shortcode_atts( array(
      'link' => '',
      'change_hover_color' => ''
   ), $atts ) );

    return "<div class='col-md-2 col-sm-4 col-xs-4'>
            <a href='https://www.behance.net/{$link}'>
                <?xml version='1.0' encoding='iso-8859-1'?>
                <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                <!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>
                <svg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'
                     width='430.123px' height='430.123px' viewBox='0 0 430.123 430.123' style='enable-background:new 0 0 430.123 430.123;''
                     xml:space='preserve' class='social-icons {$change_hover_color}'>
                <g>
                    <path id='Behance' d='M388.432,119.12H280.659V92.35h107.782v26.77H388.432z M208.912,228.895
                        c6.954,10.771,10.429,23.849,10.429,39.203c0,15.878-3.918,30.122-11.889,42.704c-5.071,8.326-11.367,15.359-18.932,21.021
                        c-8.52,6.548-18.607,11.038-30.203,13.437c-11.633,2.403-24.224,3.617-37.787,3.617H0V81.247h129.25
                        c32.579,0.53,55.676,9.969,69.315,28.506c8.184,11.369,12.239,25.011,12.239,40.868c0,16.362-4.104,29.454-12.368,39.401
                        c-4.597,5.577-11.388,10.65-20.378,15.229C191.675,210.236,202.007,218.086,208.912,228.895z M61.722,186.76h56.632
                        c11.638,0,21.046-2.212,28.292-6.634c7.241-4.415,10.854-12.263,10.854-23.531c0-12.449-4.784-20.712-14.375-24.689
                        c-8.244-2.763-18.792-4.186-31.591-4.186H61.722V186.76z M162.953,264.275c0-13.902-5.682-23.513-17.023-28.67
                        c-6.342-2.931-15.29-4.429-26.763-4.536H61.722v71.322h56.556c11.619,0,20.612-1.521,27.102-4.694
                        C157.084,291.863,162.953,280.76,162.953,264.275z M428.419,220.736c1.302,8.756,1.891,21.46,1.652,38.065H290.493
                        c0.77,19.266,7.421,32.739,20.035,40.449c7.607,4.835,16.83,7.196,27.63,7.196c11.388,0,20.67-2.879,27.815-8.797
                        c3.893-3.137,7.327-7.565,10.296-13.152h51.16c-1.34,11.379-7.5,22.92-18.57,34.648c-17.151,18.641-41.205,27.988-72.097,27.988
                        c-25.52,0-48.011-7.883-67.533-23.592C249.772,307.777,240,282.211,240,246.746c0-33.257,8.773-58.712,26.378-76.43
                        c17.67-17.751,40.474-26.586,68.583-26.586c16.661,0,31.68,2.978,45.079,8.965c13.357,5.993,24.396,15.425,33.09,28.388
                        C420.998,192.499,426.058,205.699,428.419,220.736z M378.062,225.73c-0.938-13.322-5.386-23.405-13.395-30.296
                        c-7.943-6.91-17.866-10.379-29.706-10.379c-12.886,0-22.836,3.708-29.906,10.996c-7.118,7.273-11.547,17.161-13.362,29.68H378.062
                        L378.062,225.73z'/>
                </g>
                </svg>
            </a>
            </div>";

}

add_action( 'vc_before_init', 'behance_function' );
function behance_function() {
   vc_map( array(
      "name" => __( "Behance", "adaptive" ),
      "base" => "behance",
      "class" => "",
      "category" => __( "Social", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
            array(
            "type" => "textfield",
            "holder" => "a",
            "class" => "",
            "heading" => __( "Username", "adaptive" ),
            "param_name" => "link",
            "value" => "",
            "description" => __( "Enter your username. All lowercase letters and no spaces.", "adaptive" )
         ),
            array(
            "type" => "textfield",
            "holder" => "a",
            "class" => "",
            "heading" => __( "Change hover color", "adaptive" ),
            "param_name" => "change_hover_color",
            "value" => "",
            "description" => __( "To change hover color of button enter 'iconhoverblue' for blue color, or 'iconhovergreen' for green color.", "adaptive" )
         ),
      )
   ) );
}


//facebook - shortcode
add_shortcode('facebook','facebook_simple_shortcode');
function facebook_simple_shortcode($atts) {

	extract( shortcode_atts( array(
      'link' => '',
      'change_hover_color' => ''
   ), $atts ) );

    return "<div class='col-md-2 col-sm-4 col-xs-4'>
            <a href='https://www.facebook.com/{$link}'><?xml version='1.0' encoding='iso-8859-1'?>
                <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                <!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>
                <svg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'
                     width='430.113px' height='430.114px' viewBox='0 0 430.113 430.114' style='enable-background:new 0 0 430.113 430.114;'
                     xml:space='preserve' class='social-icons {$change_hover_color}'>
                <g>
                    <path id='Facebook' d='M158.081,83.3c0,10.839,0,59.218,0,59.218h-43.385v72.412h43.385v215.183h89.122V214.936h59.805
                        c0,0,5.601-34.721,8.316-72.685c-7.784,0-67.784,0-67.784,0s0-42.127,0-49.511c0-7.4,9.717-17.354,19.321-17.354
                        c9.586,0,29.818,0,48.557,0c0-9.859,0-43.924,0-75.385c-25.016,0-53.476,0-66.021,0C155.878-0.004,158.081,72.48,158.081,83.3z'/>
                </g>
                </svg>
            </a>
            </div>";

}

add_action( 'vc_before_init', 'facebook_function' );
function facebook_function() {
   vc_map( array(
      "name" => __( "Facebook", "adaptive" ),
      "base" => "facebook",
      "class" => "",
      "category" => __( "Social", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
            array(
            "type" => "textfield",
            "holder" => "a",
            "class" => "",
            "heading" => __( "Username", "adaptive" ),
            "param_name" => "link",
            "value" => "",
            "description" => __( "Enter your username. All lowercase letters and no spaces.", "adaptive" )
         ),
            array(
            "type" => "textfield",
            "holder" => "a",
            "class" => "",
            "heading" => __( "Change hover color", "adaptive" ),
            "param_name" => "change_hover_color",
            "value" => "",
            "description" => __( "To change hover color of button enter 'iconhoverblue' for blue color, or 'iconhovergreen' for green color.", "adaptive" )
         ),
      )
   ) );
}


//twitter - shortcode
add_shortcode('twitter','twitter_simple_shortcode');
function twitter_simple_shortcode($atts) {

	extract( shortcode_atts( array(
      'link' => '',
      'change_hover_color' => ''
   ), $atts ) );

    return "<div class='col-md-2 col-sm-4 col-xs-4'>
            <a href='https://twitter.com/{$link}'><?xml version='1.0' encoding='iso-8859-1'?>
                <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                <!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>
                <svg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'
                     width='90px' height='90px' viewBox='0 0 90 90' style='enable-background:new 0 0 90 90;' xml:space='preserve' class='social-icons {$change_hover_color}'>
                <g>
                    <path id='Twitter' d='M67.261,67.496H45.054c-3.087,0-5.712-1.08-7.869-3.25c-2.167-2.172-3.238-4.797-3.238-7.898v-7.904H65.59
                        c2.854,0,5.312-1.025,7.354-3.062c2.041-2.054,3.066-4.509,3.066-7.366c0-2.867-1.025-5.319-3.072-7.366
                        c-2.049-2.042-4.515-3.066-7.381-3.066H33.946V11.254c0-3.09-1.102-5.735-3.29-7.939C28.478,1.107,25.842,0,22.782,0
                        c-3.146,0-5.825,1.091-8.004,3.25C12.591,5.416,11.5,8.084,11.5,11.264v45.089c0,9.274,3.278,17.197,9.837,23.773
                        C27.901,86.715,35.814,90,45.066,90h22.203c3.082,0,5.729-1.107,7.93-3.314c2.203-2.197,3.302-4.849,3.302-7.936
                        c0-3.088-1.099-5.734-3.302-7.941C72.997,68.607,70.347,67.496,67.261,67.496z'/>
                </g>
                </svg>
            </a>
            </div>";

}

add_action( 'vc_before_init', 'twitter_function' );
function twitter_function() {
   vc_map( array(
      "name" => __( "Twitter", "adaptive" ),
      "base" => "twitter",
      "class" => "",
      "category" => __( "Social", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
            array(
            "type" => "textfield",
            "holder" => "a",
            "class" => "",
            "heading" => __( "Username", "adaptive" ),
            "param_name" => "link",
            "value" => "",
            "description" => __( "Enter your username. All lowercase letters and no spaces.", "adaptive" )
         ),
            array(
            "type" => "textfield",
            "holder" => "a",
            "class" => "",
            "heading" => __( "Change hover color", "adaptive" ),
            "param_name" => "change_hover_color",
            "value" => "",
            "description" => __( "To change hover color of button enter 'iconhoverblue' for blue color, or 'iconhovergreen' for green color.", "adaptive" )
         ),
      )
   ) );
}











?>
