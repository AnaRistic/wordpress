<?php

/******************************************************************************/
/* OurServices - Shortcode */
/******************************************************************************/

add_shortcode('our_services', 'our_services_simple_shortcode');
function our_services_simple_shortcode($atts) {

    $atts = vc_map_get_attributes( 'our_services', $atts );
    extract( $atts );

    $sector = vc_param_group_parse_atts( $atts['sector'] );

    $title1 = (isset($sector[0]['title'])) ? $sector[0]['title'] : '';
    $title2 = (isset($sector[1]['title'])) ? $sector[1]['title'] : '';
    $title3 = (isset($sector[2]['title'])) ? $sector[2]['title'] : '';
    $title4 = (isset($sector[3]['title'])) ? $sector[3]['title'] : '';
    $title5 = (isset($sector[4]['title'])) ? $sector[4]['title'] : '';
    $title6 = (isset($sector[5]['title'])) ? $sector[5]['title'] : '';

    $content1 = (isset($sector[0]['content'])) ? $sector[0]['content'] : '';
    $content2 = (isset($sector[1]['content'])) ? $sector[1]['content'] : '';
    $content3 = (isset($sector[2]['content'])) ? $sector[2]['content'] : '';
    $content4 = (isset($sector[3]['content'])) ? $sector[3]['content'] : '';
    $content5 = (isset($sector[4]['content'])) ? $sector[4]['content'] : '';
    $content6 = (isset($sector[5]['content'])) ? $sector[5]['content'] : '';

    $class_1 = $class_2 = $class_3 = $class_4 = $class_5 = $class_6 = 'display-none';

    $nummber_of_sectors = count($sector);
    switch ($nummber_of_sectors) {
        case 1:
            $sector_class = 'col-md-12';
            $class_1 = 'display-block';
            break;
        case 2:
            $sector_class = 'col-md-6';
            $class_1 = $class_2 = 'display-block';
            break;
        case 3:
            $sector_class = 'col-md-4';
            $class_1 = $class_2 = $class_3 = 'display-block';
            break;
        case 4:
            $sector_class = 'col-md-3';
            $class_1 = $class_2 = $class_3 = $class_4 = 'display-block';
            break;
        case 5:
            $sector_class = 'col-md-2 services-width';
            $class_1 = $class_2 = $class_3 = $class_4 = $class_5 = 'display-block';
            break;
        case 6 || '> 6':
            $sector_class = 'col-md-2';
            $class_1 = $class_2 = $class_3 = $class_4 = $class_5 = $class_6 = 'display-block';
            break;
        default:
            $sector_class = '';
            break;
    }

    if (isset($img1)) {
        $img1 = wp_get_attachment_image_src($sector[0]['image_url'], "small");
        $imgSrc1 = $img1[0];
    } else {
        $imgSrc1 = IMAGES.'/tap.svg';
    }

    if (isset($img2)) {
        $img2 = wp_get_attachment_image_src($sector[1]['image_url'], "small");
        $imgSrc2 = $img2[0];
    } else {
        $imgSrc2 = IMAGES.'/settings.svg';
    }

    if (isset($img3)) {
        $img3 = wp_get_attachment_image_src($sector[2]['image_url'], "small");
        $imgSrc3 = $img3[0];
    } else {
        $imgSrc3 = IMAGES.'/support.svg';
    }

    if (isset($img4)) {
        $img4 = wp_get_attachment_image_src($sector[3]['image_url'], "small");
        $imgSrc4 = $img4[0];
    } else {
        $imgSrc4 = IMAGES.'/tap.svg';
    }

    if (isset($img5)) {
        $img5 = wp_get_attachment_image_src($sector[4]['image_url'], "small");
        $imgSrc5 = $img5[0];
    } else {
        $imgSrc5 = IMAGES.'/settings.svg';
    }

    if (isset($img6)) {
        $img6 = wp_get_attachment_image_src($sector[5]['image_url'], "small");
        $imgSrc6 = $img6[0];
    } else {
        $imgSrc6 = IMAGES.'/support.svg';
    }


    return "<div class='service-header'><span>{$main_title}</span></div>

                <div class='row text-center services-row-width'>
                    <div class='service-col {$sector_class} {$class_1}'>
                        <a href=''>
                            <img src='{$imgSrc1}' class='icon'>
                            <h3>{$title1}</h3>
                            <p>{$content1}</p>
                        </a>
                    </div>

                    <div class='service-col {$sector_class} {$class_2}'>
                        <a href=''>
                            <img src='{$imgSrc2}' class='icon'>
                            <h3>{$title2}</h3>
                            <p>{$content2}</p>
                        </a>
                    </div>
        
                    <div class='service-col {$sector_class} {$class_3}'>
                        <a href=''>
                            <img src='{$imgSrc3}' class='icon'>
                            <h3>{$title3}</h3>
                            <p>{$content3}</p>
                        </a>
                    </div>

                    <div class='service-col {$sector_class} {$class_4}'>
                        <a href=''>
                            <img src='{$imgSrc4}' class='icon'>
                            <h3>{$title4}</h3>
                            <p>{$content4}</p>
                        </a>
                    </div>

                    <div class='service-col {$sector_class} {$class_5}'>
                        <a href=''>
                            <img src='{$imgSrc5}' class='icon'>
                            <h3>{$title5}</h3>
                            <p>{$content5}</p>
                        </a>
                    </div>

                    <div class='service-col {$sector_class} {$class_6}'>
                        <a href=''>
                            <img src='{$imgSrc6}' class='icon'>
                            <h3>{$title6}</h3>
                            <p>{$content6}</p>
                        </a>
                    </div>
                </div> ";
}

add_action( 'vc_before_init', 'our_services_function' );
function our_services_function() {
   vc_map( array(
      "name" => __( "Our Services", "adaptive" ),
      "base" => "our_services",
      "class" => "",
      "category" => __( "Content", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Main Title", "adaptive" ),
            "param_name" => "main_title",
            "value" => __( "Our Services", "adaptive" ),
            "description" => __( "Enter main title name.", "adaptive" )
         ),
         // params group
            array(
                'type' => 'param_group',
                "heading" => __("Add sectors on +", "adaptive" ),
                'value' => '',
                'param_name' => 'sector',
                "description" => __( "Note: Max number of sectors is 6!", "adaptive" ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => __( 'Web Desingn', 'adaptive' ),
                        'heading' => __('Enter title', 'adaptive'),
                        'param_name' => 'title',
                    ),
                    array(
                        'type' => 'textarea',
                        'value' => __( 'Lorem ipsum dolor sit amet, consectetur adipisacing elit. Vivamus congue euerat at mattis.', 'adaptive' ),
                        'heading' => __('Enter content', 'adaptive'),
                        'param_name' => 'content',
                    ),
                    array(
                        "type" => "attach_image",
                        "heading" => __("Image", "js_composer"),
                        "param_name" => "image_url",
                        "description" => __("Choose image...", "js_composer")
                    ),
                )
            ),
        )
   ) );
}