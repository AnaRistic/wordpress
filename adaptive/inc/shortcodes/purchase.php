<?php

/******************************************************************************/
/* Purchase - Shortcode */
/******************************************************************************/

add_shortcode( 'purchase', 'purchase_simple_shortcode' );
function purchase_simple_shortcode( $atts, $content ) { 

   $atts = vc_map_get_attributes( 'purchase', $atts );
    extract( $atts );

    $img = wp_get_attachment_image_src($atts["image_url"], "large");
    if ($img) {
       $imgSrc = $img[0];
    } else {
        $imgSrc = IMAGES.'/purchase-background.jpg';
    }
  
   return "<div class='purchase' style='background: url({$imgSrc});'>
            <div class='purchaseDiv headerFP'>
              <h1>{$title}</h1>
              <p>{$content}</p>
              <a href='' class='btn contactButton margin0' style='background-color:{$button_color}'>VIEW PROJECT</a>
            </div>
          </div>";
}

add_action( 'vc_before_init', 'purchase_function' );
function purchase_function() {
   vc_map( array(
      "name" => __( "Purchase", "adaptive" ),
      "base" => "purchase",
      "class" => "purchase",
      "category" => __( "Content", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Title", "adaptive" ),
            "param_name" => "title",
            "value" => __( "Content Timeline for Wordpress", "adaptive" ),
            "description" => __( "Enter title name.", "adaptive" )
         ),
         array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Button background color", "adaptive" ),
            "param_name" => "button_color",
            "value" => '#ca1f5a',
            "description" => __( "Choose background button color", "adaptive" )
         ),
         array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Content", "adaptive" ),
            "param_name" => "content",
            "value" => __( "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis dui sodales, vuliputate odi vitae, scelerisque dolor. Lorem ipsum dolor sit amet.", "adaptive" ),
            "description" => __( "Enter content.", "adaptive" )
         ),
         array(
            "type" => "attach_image",
            "heading" => __("Image", "js_composer"),
            "holder" => "div",
            "class" => "",
            "param_name" => "image_url",
            "description" => __("Your desc", "js_composer")
         ),
      )
   ) );
}