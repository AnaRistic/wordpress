<?php

/******************************************************************************/
/* OurTeam - Shortcode */
/******************************************************************************/

add_shortcode('our_team', 'our_team_simple_shortcode');
function our_team_simple_shortcode($atts) {

    $atts = vc_map_get_attributes( 'our_team', $atts );
    extract( $atts );

    $members = vc_param_group_parse_atts( $atts['members'] );

    $title1 = (isset($members[0]['title'])) ? $members[0]['title'] : '';
    $title2 = (isset($members[1]['title'])) ? $members[1]['title'] : '';
    $title3 = (isset($members[2]['title'])) ? $members[2]['title'] : '';
    $title4 = (isset($members[3]['title'])) ? $members[3]['title'] : '';
    $title5 = (isset($members[4]['title'])) ? $members[4]['title'] : '';
    $title6 = (isset($members[5]['title'])) ? $members[5]['title'] : '';
    $title7 = (isset($members[6]['title'])) ? $members[6]['title'] : '';
    $title8 = (isset($members[7]['title'])) ? $members[7]['title'] : '';
    $title9 = (isset($members[8]['title'])) ? $members[8]['title'] : '';
    $title10 = (isset($members[9]['title'])) ? $members[9]['title'] : '';
    $title11 = (isset($members[10]['title'])) ? $members[10]['title'] : '';
    $title12 = (isset($members[11]['title'])) ? $members[11]['title'] : '';

    $content1 = (isset($members[0]['content'])) ? $members[0]['content'] : '';
    $content2 = (isset($members[1]['content'])) ? $members[1]['content'] : '';
    $content3 = (isset($members[2]['content'])) ? $members[2]['content'] : '';
    $content4 = (isset($members[3]['content'])) ? $members[3]['content'] : '';
    $content5 = (isset($members[4]['content'])) ? $members[4]['content'] : '';
    $content6 = (isset($members[5]['content'])) ? $members[5]['content'] : '';
    $content7 = (isset($members[6]['content'])) ? $members[6]['content'] : '';
    $content8 = (isset($members[7]['content'])) ? $members[7]['content'] : '';
    $content9 = (isset($members[8]['content'])) ? $members[8]['content'] : '';
    $content10 = (isset($members[9]['content'])) ? $members[9]['content'] : '';
    $content11 = (isset($members[10]['content'])) ? $members[10]['content'] : '';
    $content12 = (isset($members[11]['content'])) ? $members[11]['content'] : '';

    $facebook_username1 = (isset($members[0]['facebook_username'])) ? $members[0]['facebook_username'] : '';
    $facebook_username2 = (isset($members[1]['facebook_username'])) ? $members[1]['facebook_username'] : '';
    $facebook_username3 = (isset($members[2]['facebook_username'])) ? $members[2]['facebook_username'] : '';
    $facebook_username4 = (isset($members[3]['facebook_username'])) ? $members[3]['facebook_username'] : '';
    $facebook_username5 = (isset($members[4]['facebook_username'])) ? $members[4]['facebook_username'] : '';
    $facebook_username6 = (isset($members[5]['facebook_username'])) ? $members[5]['facebook_username'] : '';
    $facebook_username7 = (isset($members[6]['facebook_username'])) ? $members[6]['facebook_username'] : '';
    $facebook_username8 = (isset($members[7]['facebook_username'])) ? $members[7]['facebook_username'] : '';
    $facebook_username9 = (isset($members[8]['facebook_username'])) ? $members[8]['facebook_username'] : '';
    $facebook_username10 = (isset($members[9]['facebook_username'])) ? $members[9]['facebook_username'] : '';
    $facebook_username11 = (isset($members[10]['facebook_username'])) ? $members[10]['facebook_username'] : '';
    $facebook_username12 = (isset($members[11]['facebook_username'])) ? $members[11]['facebook_username'] : '';

    $twitter_username1 = (isset($members[0]['twitter_username'])) ? $members[0]['twitter_username'] : '';
    $twitter_username2 = (isset($members[1]['twitter_username'])) ? $members[1]['twitter_username'] : '';
    $twitter_username3 = (isset($members[2]['twitter_username'])) ? $members[2]['twitter_username'] : '';
    $twitter_username4 = (isset($members[3]['twitter_username'])) ? $members[3]['twitter_username'] : '';
    $twitter_username5 = (isset($members[4]['twitter_username'])) ? $members[4]['twitter_username'] : '';
    $twitter_username6 = (isset($members[5]['twitter_username'])) ? $members[5]['twitter_username'] : '';
    $twitter_username7 = (isset($members[6]['twitter_username'])) ? $members[6]['twitter_username'] : '';
    $twitter_username8 = (isset($members[7]['twitter_username'])) ? $members[7]['twitter_username'] : '';
    $twitter_username9 = (isset($members[8]['twitter_username'])) ? $members[8]['twitter_username'] : '';
    $twitter_username10 = (isset($members[9]['twitter_username'])) ? $members[9]['twitter_username'] : '';
    $twitter_username11 = (isset($members[10]['twitter_username'])) ? $members[10]['twitter_username'] : '';
    $twitter_username12 = (isset($members[11]['twitter_username'])) ? $members[11]['twitter_username'] : '';

    $linkedin_username1 = (isset($members[0]['linkedin_username'])) ? $members[0]['linkedin_username'] : '';
    $linkedin_username2 = (isset($members[1]['linkedin_username'])) ? $members[1]['linkedin_username'] : '';
    $linkedin_username3 = (isset($members[2]['linkedin_username'])) ? $members[2]['linkedin_username'] : '';
    $linkedin_username4 = (isset($members[3]['linkedin_username'])) ? $members[3]['linkedin_username'] : '';
    $linkedin_username5 = (isset($members[4]['linkedin_username'])) ? $members[4]['linkedin_username'] : '';
    $linkedin_username6 = (isset($members[5]['linkedin_username'])) ? $members[5]['linkedin_username'] : '';
    $linkedin_username7 = (isset($members[6]['linkedin_username'])) ? $members[6]['linkedin_username'] : '';
    $linkedin_username8 = (isset($members[7]['linkedin_username'])) ? $members[7]['linkedin_username'] : '';
    $linkedin_username9 = (isset($members[8]['linkedin_username'])) ? $members[8]['linkedin_username'] : '';
    $linkedin_username10 = (isset($members[9]['linkedin_username'])) ? $members[9]['linkedin_username'] : '';
    $linkedin_username11 = (isset($members[10]['linkedin_username'])) ? $members[10]['linkedin_username'] : '';
    $linkedin_username12 = (isset($members[11]['linkedin_username'])) ? $members[11]['linkedin_username'] : '';

    $youtube_username1 = (isset($members[0]['youtube_username'])) ? $members[0]['youtube_username'] : '';
    $youtube_username2 = (isset($members[1]['youtube_username'])) ? $members[1]['youtube_username'] : '';
    $youtube_username3 = (isset($members[2]['youtube_username'])) ? $members[2]['youtube_username'] : '';
    $youtube_username4 = (isset($members[3]['youtube_username'])) ? $members[3]['youtube_username'] : '';
    $youtube_username5 = (isset($members[4]['youtube_username'])) ? $members[4]['youtube_username'] : '';
    $youtube_username6 = (isset($members[5]['youtube_username'])) ? $members[5]['youtube_username'] : '';
    $youtube_username7 = (isset($members[6]['youtube_username'])) ? $members[6]['youtube_username'] : '';
    $youtube_username8 = (isset($members[7]['youtube_username'])) ? $members[7]['youtube_username'] : '';
    $youtube_username9 = (isset($members[8]['youtube_username'])) ? $members[8]['youtube_username'] : '';
    $youtube_username10 = (isset($members[9]['youtube_username'])) ? $members[9]['youtube_username'] : '';
    $youtube_username11 = (isset($members[10]['youtube_username'])) ? $members[10]['youtube_username'] : '';
    $youtube_username12 = (isset($members[11]['youtube_username'])) ? $members[11]['youtube_username'] : '';

    $instagram_username1 = (isset($members[0]['instagram_username'])) ? $members[0]['instagram_username'] : '';
    $instagram_username2 = (isset($members[1]['instagram_username'])) ? $members[1]['instagram_username'] : '';
    $instagram_username3 = (isset($members[2]['instagram_username'])) ? $members[2]['instagram_username'] : '';
    $instagram_username4 = (isset($members[3]['instagram_username'])) ? $members[3]['instagram_username'] : '';
    $instagram_username5 = (isset($members[4]['instagram_username'])) ? $members[4]['instagram_username'] : '';
    $instagram_username6 = (isset($members[5]['instagram_username'])) ? $members[5]['instagram_username'] : '';
    $instagram_username7 = (isset($members[6]['instagram_username'])) ? $members[6]['instagram_username'] : '';
    $instagram_username8 = (isset($members[7]['instagram_username'])) ? $members[7]['instagram_username'] : '';
    $instagram_username9 = (isset($members[8]['instagram_username'])) ? $members[8]['instagram_username'] : '';
    $instagram_username10 = (isset($members[9]['instagram_username'])) ? $members[9]['instagram_username'] : '';
    $instagram_username11 = (isset($members[10]['instagram_username'])) ? $members[10]['instagram_username'] : '';
    $instagram_username12 = (isset($members[11]['instagram_username'])) ? $members[11]['instagram_username'] : '';

    $snapchat_username1 = (isset($members[0]['snapchat_username'])) ? $members[0]['snapchat_username'] : '';
    $snapchat_username2 = (isset($members[1]['snapchat_username'])) ? $members[1]['snapchat_username'] : '';
    $snapchat_username3 = (isset($members[2]['snapchat_username'])) ? $members[2]['snapchat_username'] : '';
    $snapchat_username4 = (isset($members[3]['snapchat_username'])) ? $members[3]['snapchat_username'] : '';
    $snapchat_username5 = (isset($members[4]['snapchat_username'])) ? $members[4]['snapchat_username'] : '';
    $snapchat_username6 = (isset($members[5]['snapchat_username'])) ? $members[5]['snapchat_username'] : '';
    $snapchat_username7 = (isset($members[6]['snapchat_username'])) ? $members[6]['snapchat_username'] : '';
    $snapchat_username8 = (isset($members[7]['snapchat_username'])) ? $members[7]['snapchat_username'] : '';
    $snapchat_username9 = (isset($members[8]['snapchat_username'])) ? $members[8]['snapchat_username'] : '';
    $snapchat_username10 = (isset($members[9]['snapchat_username'])) ? $members[9]['snapchat_username'] : '';
    $snapchat_username11 = (isset($members[10]['snapchat_username'])) ? $members[10]['snapchat_username'] : '';
    $snapchat_username12 = (isset($members[11]['snapchat_username'])) ? $members[11]['snapchat_username'] : '';
    
    if (isset($img1)) {
        $img1 = wp_get_attachment_image_src($members[0]['image_url'], "medium");
        $imgSrc1 = $img1[0];
    } else {
        $imgSrc1 = IMAGES.'/SquareTeam5.jpg';
    }

    if (isset($img2)) {
        $img2 = wp_get_attachment_image_src($members[1]['image_url'], "medium");
        $imgSrc2 = $img2[0];
    } else {
        $imgSrc2 = IMAGES.'/SquareTeam4.jpg';
    }

    if (isset($img3)) {
        $img3 = wp_get_attachment_image_src($members[2]['image_url'], "medium");
        $imgSrc3 = $img3[0];
    } else {
        $imgSrc3 = IMAGES.'/SquareTeam3.jpg';
    }

    if (isset($img4)) {
        $img4 = wp_get_attachment_image_src($members[3]['image_url'], "medium");
        $imgSrc4 = $img4[0];
    } else {
        $imgSrc4 = IMAGES.'/SquareTeam2.jpg';
    }

    if (isset($img5)) {
        $img5 = wp_get_attachment_image_src($members[4]['image_url'], "medium");
        $imgSrc5 = $img5[0];
    } else {
        $imgSrc5 = IMAGES.'/SquareTeam1.jpg';
    }

    if (isset($img6)) {
        $img6 = wp_get_attachment_image_src($members[5]['image_url'], "medium");
        $imgSrc6 = $img6[0];
    } else {
        $imgSrc6 = IMAGES.'/SquareTeam8.jpg';
    }

    if (isset($img7)) {
        $img7 = wp_get_attachment_image_src($members[6]['image_url'], "medium");
        $imgSrc7 = $img7[0];
    } else {
        $imgSrc7 = IMAGES.'/SquareTeam7.jpg';
    }

    if (isset($img8)) {
        $img8 = wp_get_attachment_image_src($members[7]['image_url'], "medium");
        $imgSrc8 = $img8[0];
    } else {
        $imgSrc8 = IMAGES.'/SquareTeam6.jpg';
    }

    if (isset($img9)) {
        $img9 = wp_get_attachment_image_src($members[8]['image_url'], "medium");
        $imgSrc9 = $img9[0];
    } else {
        $imgSrc9 = IMAGES.'/SquareTeam5.jpg';
    }
    
    if (isset($img10)) {
        $img10 = wp_get_attachment_image_src($members[9]['image_url'], "medium");
        $imgSrc10 = $img10[0];
    } else {
        $imgSrc10 = IMAGES.'/SquareTeam4.jpg';
    }
    
    if (isset($img11)) {
        $img11 = wp_get_attachment_image_src($members[10]['image_url'], "medium");
        $imgSrc11 = $img11[0];
    } else {
        $imgSrc11 = IMAGES.'/SquareTeam3.jpg';
    }

    if (isset($img12)) {
        $img12 = wp_get_attachment_image_src($members[11]['image_url'], "medium");
        $imgSrc12 = $img12[0];
    } else {
        $imgSrc12 = IMAGES.'/SquareTeam2.jpg';
    }


    $icons1 = isset($members[0]['fa_icons']) ? $members[0]['fa_icons'] : '';
    $icon1 = explode(",", $icons1);

    foreach ($icon1 as $icon) {
        if ($icon == "facebook") { $facebook1 = true; }
        if ($icon == "twitter") { $twitter1 = true; }
        if ($icon == "linkedin") { $linkedin1 = true; }
        if ($icon == "youtube") { $youtube1 = true; }
        if ($icon == "instagram") { $instagram1 = true; }
        if ($icon == "snapchat") { $snapchat1 = true; }
    }
    $facebook1 = (isset($facebook1) && $facebook1 = true) ? $facebook1 = 'display-inline-block' : 'display-none';
    $twitter1 = (isset($twitter1) && $twitter1 = true) ? $twitter1 = 'display-inline-block' : 'display-none';
    $linkedin1 = (isset($linkedin1) && $linkedin1 = true) ? $linkedin1 = 'display-inline-block' : 'display-none';
    $youtube1 = (isset($youtube1) && $youtube1 = true) ? $youtube1 = 'display-inline-block' : 'display-none';
    $instagram1 = (isset($instagram1) && $instagram1 = true) ? $instagram1 = 'display-inline-block' : 'display-none';
    $snapchat1 = (isset($snapchat1) && $snapchat1 = true) ? $snapchat1 = 'display-inline-block' : 'display-none';

    $icons2 = isset($members[1]['fa_icons']) ? $members[1]['fa_icons'] : '';
    $icon2 = explode(",", $icons2);

    foreach ($icon2 as $icon) {
        if ($icon == "facebook") { $facebook2 = true; }
        if ($icon == "twitter") { $twitter2 = true; }
        if ($icon == "linkedin") { $linkedin2 = true; }
        if ($icon == "youtube") { $youtube2 = true; }
        if ($icon == "instagram") { $instagram2 = true; }
        if ($icon == "snapchat") { $snapchat2 = true; }
    }
    $facebook2 = (isset($facebook2) && $facebook2 = true) ? $facebook2 = 'display-inline-block' : 'display-none';
    $twitter2 = (isset($twitter2) && $twitter2 = true) ? $twitter2 = 'display-inline-block' : 'display-none';
    $linkedin2 = (isset($linkedin2) && $linkedin2 = true) ? $linkedin2 = 'display-inline-block' : 'display-none';
    $youtube2 = (isset($youtube2) && $youtube2 = true) ? $youtube2 = 'display-inline-block' : 'display-none';
    $instagram2 = (isset($instagram2) && $instagram2 = true) ? $instagram2 = 'display-inline-block' : 'display-none';
    $snapchat2 = (isset($snapchat2) && $snapchat2 = true) ? $snapchat2 = 'display-inline-block' : 'display-none';

    $icons3 = isset($members[2]['fa_icons']) ? $members[2]['fa_icons'] : '';
    $icon3 = explode(",", $icons3);

    foreach ($icon3 as $icon) {
        if ($icon == "facebook") { $facebook3 = true; }
        if ($icon == "twitter") { $twitter3 = true; }
        if ($icon == "linkedin") { $linkedin3 = true; }
        if ($icon == "youtube") { $youtube3 = true; }
        if ($icon == "instagram") { $instagram3 = true; }
        if ($icon == "snapchat") { $snapchat3 = true; }
    }
    $facebook3 = (isset($facebook3) && $facebook3 = true) ? $facebook3 = 'display-inline-block' : 'display-none';
    $twitter3 = (isset($twitter3) && $twitter3 = true) ? $twitter3 = 'display-inline-block' : 'display-none';
    $linkedin3 = (isset($linkedin3) && $linkedin3 = true) ? $linkedin3 = 'display-inline-block' : 'display-none';
    $youtube3 = (isset($youtube3) && $youtube3 = true) ? $youtube3 = 'display-inline-block' : 'display-none';
    $instagram3 = (isset($instagram3) && $instagram3 = true) ? $instagram3 = 'display-inline-block' : 'display-none';
    $snapchat3 = (isset($snapchat3) && $snapchat3 = true) ? $snapchat3 = 'display-inline-block' : 'display-none';

    $icons4 = isset($members[3]['fa_icons']) ? $members[3]['fa_icons'] : '';
    $icon4 = explode(",", $icons4);

    foreach ($icon4 as $icon) {
        if ($icon == "facebook") { $facebook4 = true; }
        if ($icon == "twitter") { $twitter4 = true; }
        if ($icon == "linkedin") { $linkedin4 = true; }
        if ($icon == "youtube") { $youtube4 = true; }
        if ($icon == "instagram") { $instagram4 = true; }
        if ($icon == "snapchat") { $snapchat4 = true; }
    }
    $facebook4 = (isset($facebook4) && $facebook4 = true) ? $facebook4 = 'display-inline-block' : 'display-none';
    $twitter4 = (isset($twitter4) && $twitter4 = true) ? $twitter4 = 'display-inline-block' : 'display-none';
    $linkedin4 = (isset($linkedin4) && $linkedin4 = true) ? $linkedin4 = 'display-inline-block' : 'display-none';
    $youtube4 = (isset($youtube4) && $youtube4 = true) ? $youtube4 = 'display-inline-block' : 'display-none';
    $instagram4 = (isset($instagram4) && $instagram4 = true) ? $instagram4 = 'display-inline-block' : 'display-none';
    $snapchat4 = (isset($snapchat4) && $snapchat4 = true) ? $snapchat4 = 'display-inline-block' : 'display-none';

    $icons5 = isset($members[4]['fa_icons']) ? $members[4]['fa_icons'] : '';
    $icon5 = explode(",", $icons5);

    foreach ($icon5 as $icon) {
        if ($icon == "facebook") { $facebook5 = true; }
        if ($icon == "twitter") { $twitter5 = true; }
        if ($icon == "linkedin") { $linkedin5 = true; }
        if ($icon == "youtube") { $youtube5 = true; }
        if ($icon == "instagram") { $instagram5 = true; }
        if ($icon == "snapchat") { $snapchat5 = true; }
    }
    $facebook5 = (isset($facebook5) && $facebook5 = true) ? $facebook5 = 'display-inline-block' : 'display-none';
    $twitter5 = (isset($twitter5) && $twitter5 = true) ? $twitter5 = 'display-inline-block' : 'display-none';
    $linkedin5 = (isset($linkedin5) && $linkedin5 = true) ? $linkedin5 = 'display-inline-block' : 'display-none';
    $youtube5 = (isset($youtube5) && $youtube5 = true) ? $youtube5 = 'display-inline-block' : 'display-none';
    $instagram5 = (isset($instagram5) && $instagram5 = true) ? $instagram5 = 'display-inline-block' : 'display-none';
    $snapchat5 = (isset($snapchat5) && $snapchat5 = true) ? $snapchat5 = 'display-inline-block' : 'display-none';

    $icons6 = isset($members[5]['fa_icons']) ? $members[5]['fa_icons'] : '';
    $icon6 = explode(",", $icons6);

    foreach ($icon6 as $icon) {
        if ($icon == "facebook") { $facebook6 = true; }
        if ($icon == "twitter") { $twitter6 = true; }
        if ($icon == "linkedin") { $linkedin6 = true; }
        if ($icon == "youtube") { $youtube6 = true; }
        if ($icon == "instagram") { $instagram6 = true; }
        if ($icon == "snapchat") { $snapchat6 = true; }
    }
    $facebook6 = (isset($facebook6) && $facebook6 = true) ? $facebook6 = 'display-inline-block' : 'display-none';
    $twitter6 = (isset($twitter6) && $twitter6 = true) ? $twitter6 = 'display-inline-block' : 'display-none';
    $linkedin6 = (isset($linkedin6) && $linkedin6 = true) ? $linkedin6 = 'display-inline-block' : 'display-none';
    $youtube6 = (isset($youtube6) && $youtube6 = true) ? $youtube6 = 'display-inline-block' : 'display-none';
    $instagram6 = (isset($instagram6) && $instagram6 = true) ? $instagram6 = 'display-inline-block' : 'display-none';
    $snapchat6 = (isset($snapchat6) && $snapchat6 = true) ? $snapchat6 = 'display-inline-block' : 'display-none';

    $icons7 = isset($members[6]['fa_icons']) ? $members[6]['fa_icons'] : '';
    $icon7 = explode(",", $icons7);

    foreach ($icon7 as $icon) {
        if ($icon == "facebook") { $facebook7 = true; }
        if ($icon == "twitter") { $twitter7 = true; }
        if ($icon == "linkedin") { $linkedin7 = true; }
        if ($icon == "youtube") { $youtube7 = true; }
        if ($icon == "instagram") { $instagram7 = true; }
        if ($icon == "snapchat") { $snapchat7 = true; }
    }
    $facebook7 = (isset($facebook7) && $facebook7 = true) ? $facebook7 = 'display-inline-block' : 'display-none';
    $twitter7 = (isset($twitter7) && $twitter7 = true) ? $twitter7 = 'display-inline-block' : 'display-none';
    $linkedin7 = (isset($linkedin7) && $linkedin7 = true) ? $linkedin7 = 'display-inline-block' : 'display-none';
    $youtube7 = (isset($youtube7) && $youtube7 = true) ? $youtube7 = 'display-inline-block' : 'display-none';
    $instagram7 = (isset($instagram7) && $instagram7 = true) ? $instagram7 = 'display-inline-block' : 'display-none';
    $snapchat7 = (isset($snapchat7) && $snapchat7 = true) ? $snapchat7 = 'display-inline-block' : 'display-none';

    $icons8 = isset($members[7]['fa_icons']) ? $members[7]['fa_icons'] : '';
    $icon8 = explode(",", $icons8);

    foreach ($icon8 as $icon) {
        if ($icon == "facebook") { $facebook8 = true; }
        if ($icon == "twitter") { $twitter8 = true; }
        if ($icon == "linkedin") { $linkedin8 = true; }
        if ($icon == "youtube") { $youtube8 = true; }
        if ($icon == "instagram") { $instagram8 = true; }
        if ($icon == "snapchat") { $snapchat8 = true; }
    }
    $facebook8 = (isset($facebook8) && $facebook8 = true) ? $facebook8 = 'display-inline-block' : 'display-none';
    $twitter8 = (isset($twitter8) && $twitter8 = true) ? $twitter8 = 'display-inline-block' : 'display-none';
    $linkedin8 = (isset($linkedin8) && $linkedin8 = true) ? $linkedin8 = 'display-inline-block' : 'display-none';
    $youtube8 = (isset($youtube8) && $youtube8 = true) ? $youtube8 = 'display-inline-block' : 'display-none';
    $instagram8 = (isset($instagram8) && $instagram8 = true) ? $instagram8 = 'display-inline-block' : 'display-none';
    $snapchat8 = (isset($snapchat8) && $snapchat8 = true) ? $snapchat8 = 'display-inline-block' : 'display-none';

    $icons9 = isset($members[8]['fa_icons']) ? $members[8]['fa_icons'] : '';
    $icon9 = explode(",", $icons9);

    foreach ($icon9 as $icon) {
        if ($icon == "facebook") { $facebook9 = true; }
        if ($icon == "twitter") { $twitter9 = true; }
        if ($icon == "linkedin") { $linkedin9 = true; }
        if ($icon == "youtube") { $youtube9 = true; }
        if ($icon == "instagram") { $instagram9 = true; }
        if ($icon == "snapchat") { $snapchat9 = true; }
    }
    $facebook9 = (isset($facebook9) && $facebook9 = true) ? $facebook9 = 'display-inline-block' : 'display-none';
    $twitter9 = (isset($twitter9) && $twitter9 = true) ? $twitter9 = 'display-inline-block' : 'display-none';
    $linkedin9 = (isset($linkedin9) && $linkedin9 = true) ? $linkedin9 = 'display-inline-block' : 'display-none';
    $youtube9 = (isset($youtube9) && $youtube9 = true) ? $youtube9 = 'display-inline-block' : 'display-none';
    $instagram9 = (isset($instagram9) && $instagram9 = true) ? $instagram9 = 'display-inline-block' : 'display-none';
    $snapchat9 = (isset($snapchat9) && $snapchat9 = true) ? $snapchat9 = 'display-inline-block' : 'display-none';

    $icons10 = isset($members[9]['fa_icons']) ? $members[9]['fa_icons'] : '';
    $icon10 = explode(",", $icons10);

    foreach ($icon10 as $icon) {
        if ($icon == "facebook") { $facebook10 = true; }
        if ($icon == "twitter") { $twitter10 = true; }
        if ($icon == "linkedin") { $linkedin10 = true; }
        if ($icon == "youtube") { $youtube10 = true; }
        if ($icon == "instagram") { $instagram10 = true; }
        if ($icon == "snapchat") { $snapchat10 = true; }
    }
    $facebook10 = (isset($facebook10) && $facebook10 = true) ? $facebook10 = 'display-inline-block' : 'display-none';
    $twitter10 = (isset($twitter10) && $twitter10 = true) ? $twitter10 = 'display-inline-block' : 'display-none';
    $linkedin10 = (isset($linkedin10) && $linkedin10 = true) ? $linkedin10 = 'display-inline-block' : 'display-none';
    $youtube10 = (isset($youtube10) && $youtube10 = true) ? $youtube10 = 'display-inline-block' : 'display-none';
    $instagram10 = (isset($instagram10) && $instagram10 = true) ? $instagram10 = 'display-inline-block' : 'display-none';
    $snapchat10 = (isset($snapchat10) && $snapchat10 = true) ? $snapchat10 = 'display-inline-block' : 'display-none';

    $icons11 = isset($members[10]['fa_icons']) ? $members[10]['fa_icons'] : '';
    $icon11 = explode(",", $icons11);

    foreach ($icon11 as $icon) {
        if ($icon == "facebook") { $facebook11 = true; }
        if ($icon == "twitter") { $twitter11 = true; }
        if ($icon == "linkedin") { $linkedin11 = true; }
        if ($icon == "youtube") { $youtube11 = true; }
        if ($icon == "instagram") { $instagram11 = true; }
        if ($icon == "snapchat") { $snapchat11 = true; }
    }
    $facebook11 = (isset($facebook11) && $facebook11 = true) ? $facebook11 = 'display-inline-block' : 'display-none';
    $twitter11 = (isset($twitter11) && $twitter11 = true) ? $twitter11 = 'display-inline-block' : 'display-none';
    $linkedin11 = (isset($linkedin11) && $linkedin11 = true) ? $linkedin11 = 'display-inline-block' : 'display-none';
    $youtube11 = (isset($youtube11) && $youtube11 = true) ? $youtube11 = 'display-inline-block' : 'display-none';
    $instagram11 = (isset($instagram11) && $instagram11 = true) ? $instagram11 = 'display-inline-block' : 'display-none';
    $snapchat11 = (isset($snapchat11) && $snapchat11 = true) ? $snapchat11 = 'display-inline-block' : 'display-none';

    $icons12 = isset($members[11]['fa_icons']) ? $members[11]['fa_icons'] : '';
    $icon12 = explode(",", $icons12);

    foreach ($icon12 as $icon) {
        if ($icon == "facebook") { $facebook12 = true; }
        if ($icon == "twitter") { $twitter12 = true; }
        if ($icon == "linkedin") { $linkedin12 = true; }
        if ($icon == "youtube") { $youtube12 = true; }
        if ($icon == "instagram") { $instagram12 = true; }
        if ($icon == "snapchat") { $snapchat12 = true; }
    }
    $facebook12 = (isset($facebook12) && $facebook12 = true) ? $facebook12 = 'display-inline-block' : 'display-none';
    $twitter12 = (isset($twitter12) && $twitter12 = true) ? $twitter12 = 'display-inline-block' : 'display-none';
    $linkedin12 = (isset($linkedin12) && $linkedin12 = true) ? $linkedin12 = 'display-inline-block' : 'display-none';
    $youtube12 = (isset($youtube12) && $youtube12 = true) ? $youtube12 = 'display-inline-block' : 'display-none';
    $instagram12 = (isset($instagram12) && $instagram12 = true) ? $instagram12 = 'display-inline-block' : 'display-none';
    $snapchat12 = (isset($snapchat12) && $snapchat12 = true) ? $snapchat12 = 'display-inline-block' : 'display-none';

    $member_1 = $member_2 = $member_3 = $member_4 = $member_5 = $member_6 = $member_7 = $member_8 = $member_9 = $member_10 = $member_11 = $member_12 = 'display-none';

    $nummber_of_members = count($members);
    switch ($nummber_of_members) {
        case '1':
            $class = 'col-md-12 team-correction';
            $member_1 = ' display-inline-block';
            break;
        case '2':
            $class = 'col-md-6 col-sm-6 team-correction';
            $member_1 = $member_2 = ' display-inline-block';
            break;
        case '3':
            $class = 'col-md-4 col-sm-6';
            $member_1 = $member_2 = $member_3 = ' display-inline-block';
            break;
        case '4':
            $class = 'col-md-3 col-sm-6';
            $member_1 = $member_2 = $member_3 = $member_4 = ' display-inline-block';
            break;
        case '5':
            $class = 'col-md-4 col-sm-6';
            $member_1 = $member_2 = $member_3 = $member_4 = $member_5 = ' display-inline-block';
            break;
        case '6':
            $class = 'col-md-4 col-sm-6';
            $member_1 = $member_2 = $member_3 = $member_4 = $member_5 = $member_6 = ' display-inline-block';
            break;
        case '7':
            $class = 'col-md-3 col-sm-6';
            $member_1 = $member_2 = $member_3 = $member_4 = $member_5 = $member_6 = $member_7 = ' display-inline-block';
            break;
        case '8':
            $class = 'col-md-3 col-sm-6';
            $member_1 = $member_2 = $member_3 = $member_4 = $member_5 = $member_6 = $member_7 = $member_8 = ' display-inline-block';
            break;
        case '9':
            $class = 'col-lg-2 col-md-4 col-sm-6 width';
            $member_1 = $member_2 = $member_3 = $member_4 = $member_5 = $member_6 = $member_7 = $member_8 = $member_9 = ' display-inline-block';
            break;
        case '10':
            $class = 'col-lg-2 col-md-3 col-sm-6 width';
            $member_1 = $member_2 = $member_3 = $member_4 = $member_5 = $member_6 = $member_7 = $member_8 = $member_9 = $member_10 = ' display-inline-block';
            break;
        case '11':
            $class = 'col-lg-2 col-md-3 col-sm-6';
            $member_1 = $member_2 = $member_3 = $member_4 = $member_5 = $member_6 = $member_7 = $member_8 = $member_9 = $member_10 = $member_11 = ' display-inline-block';
            break;
        case '12' || '> 12':
            $class = 'col-lg-2 col-md-3 col-sm-6';
            $member_1 = $member_2 = $member_3 = $member_4 = $member_5 = $member_6 = $member_7 = $member_8 = $member_9 = $member_10 = $member_11 = $member_12 = ' display-inline-block';
            break;
        
        default:
            $class = '';
            break;
    }

	return "<div class='service-header'><span>{$main_title}</span></div>

        <div class='row team-row'>
            	<div class='{$class} {$member_1} team'>
                	<img src='{$imgSrc1}' class='img-responsive teamImg'>
                    	<h4 class='text'>HUMAN RESOURCES</h4>
                	<div class='teamDiv'>
                    	<h2>{$title1}</h2>
                    	<p>{$content1}</p>
                        <div class='fa-icons'>
                            <a href='https://www.facebook.com/{$facebook_username1}' class='facss fa-facebook {$facebook1}'></a>
                            <a href='https://twitter.com/{$twitter_username1}' class='facss fa-twitter {$twitter1}'></a>
                            <a href='https://www.linkedin.com/{$linkedin_username1}' class='facss fa-linkedin {$linkedin1}'></a>
                            <a href='https://www.youtube.com/{$youtube_username1}' class='facss fa-youtube {$youtube1}'></a>
                            <a href='https://www.instagram.com/{$instagram_username1}' class='facss fa-instagram {$instagram1}'></a>
                            <a href='https://www.snapchat.com/{$snapchat_username1}' class='facss fa-snapchat-ghost {$snapchat1}'></a>
                        </div>
                	</div>
            	</div>
            	<div class='{$class} {$member_2} team'>
            	    <img src='{$imgSrc2}' class='img-responsive teamImg'>
            	        <h4 class='text'>HUMAN RESOURCES</h4>
            	    <div class='teamDiv'>
            	        <h2>{$title2}</h2>
            	        <p>{$content2}</p>
                        <div class='fa-icons'>
                            <a href='https://www.facebook.com/{$facebook_username2}' class='facss fa-facebook {$facebook2}'></a>
                            <a href='https://twitter.com/{$twitter_username2}' class='facss fa-twitter {$twitter2}'></a>
                            <a href='https://www.linkedin.com/{$linkedin_username2}' class='facss fa-linkedin {$linkedin2}'></a>
                            <a href='https://www.youtube.com/{$youtube_username2}' class='facss fa-youtube {$youtube2}'></a>
                            <a href='https://www.instagram.com/{$instagram_username2}' class='facss fa-instagram {$instagram2}'></a>
                            <a href='https://www.snapchat.com/{$snapchat_username2}' class='facss fa-snapchat-ghost {$snapchat2}'></a>
                        </div>
                	</div>
            	</div>
            	<div class='{$class} {$member_3} team'>
            	    <img src='{$imgSrc3}' class='img-responsive teamImg'>
            	        <h4 class='text'>HUMAN RESOURCES</h4>
            	    <div class='teamDiv'>
            	        <h2>{$title3}</h2>
            	        <p>{$content3}</p>
                        <div class='fa-icons'>
                            <a href='https://www.facebook.com/{$facebook_username3}' class='facss fa-facebook {$facebook3}'></a>
                            <a href='https://twitter.com/{$twitter_username3}' class='facss fa-twitter {$twitter3}'></a>
                            <a href='https://www.linkedin.com/{$linkedin_username3}' class='facss fa-linkedin {$linkedin3}'></a>
                            <a href='https://www.youtube.com/{$youtube_username3}' class='facss fa-youtube {$youtube3}'></a>
                            <a href='https://www.instagram.com/{$instagram_username3}' class='facss fa-instagram {$instagram3}'></a>
                            <a href='https://www.snapchat.com/{$snapchat_username3}' class='facss fa-snapchat-ghost {$snapchat3}'></a>
                        </div>
                	</div>
            	</div>
            	<div class='{$class} {$member_4} team'>
            	    <img src='{$imgSrc4}' class='img-responsive teamImg'>
            	        <h4 class='text'>HUMAN RESOURCES</h4>
            	    <div class='teamDiv'>
            	        <h2>{$title4}</h2>
            	        <p>{$content4}</p>
                        <div class='fa-icons'>
                            <a href='https://www.facebook.com/{$facebook_username4}' class='facss fa-facebook {$facebook4}'></a>
                            <a href='https://twitter.com/{$twitter_username4}' class='facss fa-twitter {$twitter4}'></a>
                            <a href='https://www.linkedin.com/{$linkedin_username4}' class='facss fa-linkedin {$linkedin4}'></a>
                            <a href='https://www.youtube.com/{$youtube_username4}' class='facss fa-youtube {$youtube4}'></a>
                            <a href='https://www.instagram.com/{$instagram_username4}' class='facss fa-instagram {$instagram4}'></a>
                            <a href='https://www.snapchat.com/{$snapchat_username4}' class='facss fa-snapchat-ghost {$snapchat4}'></a>
                        </div>
                	</div>
            	</div>
    	        <div class='{$class} {$member_5} team'>
    	            <img src='{$imgSrc5}' class='img-responsive teamImg'>
    	               <h4 class='text'>HUMAN RESOURCES</h4>
    	            <div class='teamDiv'>
    	               <h2>{$title5}</h2>
    	               <p>{$content5}</p>
                       <div class='fa-icons'>
                            <a href='https://www.facebook.com/{$facebook_username5}' class='facss fa-facebook {$facebook5}'></a>
                            <a href='https://twitter.com/{$twitter_username5}' class='facss fa-twitter {$twitter5}'></a>
                            <a href='https://www.linkedin.com/{$linkedin_username5}' class='facss fa-linkedin {$linkedin5}'></a>
                            <a href='https://www.youtube.com/{$youtube_username5}' class='facss fa-youtube {$youtube5}'></a>
                            <a href='https://www.instagram.com/{$instagram_username5}' class='facss fa-instagram {$instagram5}'></a>
                            <a href='https://www.snapchat.com/{$snapchat_username5}' class='facss fa-snapchat-ghost {$snapchat5}'></a>
                        </div>
                	</div>
            	</div>
            	<div class='{$class} {$member_6} team'>
            	    <img src='{$imgSrc6}' class='img-responsive teamImg'>
            	        <h4 class='text'>HUMAN RESOURCES</h4>
            	    <div class='teamDiv'>
            	        <h2>{$title6}</h2>
            	        <p>{$content6}</p>
                        <div class='fa-icons'>
                            <a href='https://www.facebook.com/{$facebook_username6}' class='facss fa-facebook {$facebook6}'></a>
                            <a href='https://twitter.com/{$twitter_username6}' class='facss fa-twitter {$twitter6}'></a>
                            <a href='https://www.linkedin.com/{$linkedin_username6}' class='facss fa-linkedin {$linkedin6}'></a>
                            <a href='https://www.youtube.com/{$youtube_username6}' class='facss fa-youtube {$youtube6}'></a>
                            <a href='https://www.instagram.com/{$instagram_username6}' class='facss fa-instagram {$instagram6}'></a>
                            <a href='https://www.snapchat.com/{$snapchat_username6}' class='facss fa-snapchat-ghost {$snapchat6}'></a>
                        </div>
                	</div>
            	</div>
            	<div class='{$class} {$member_7} team'>
            	    <img src='{$imgSrc7}' class='img-responsive teamImg'>
            	        <h4 class='text'>HUMAN RESOURCES</h4>
            	    <div class='teamDiv'>
            	        <h2>{$title7}</h2>
            	        <p>{$content7}</p>
                        <div class='fa-icons'>
                            <a href='https://www.facebook.com/{$facebook_username7}' class='facss fa-facebook {$facebook7}'></a>
                            <a href='https://twitter.com/{$twitter_username7}' class='facss fa-twitter {$twitter7}'></a>
                            <a href='https://www.linkedin.com/{$linkedin_username7}' class='facss fa-linkedin {$linkedin7}'></a>
                            <a href='https://www.youtube.com/{$youtube_username7}' class='facss fa-youtube {$youtube7}'></a>
                            <a href='https://www.instagram.com/{$instagram_username7}' class='facss fa-instagram {$instagram7}'></a>
                            <a href='https://www.snapchat.com/{$snapchat_username7}' class='facss fa-snapchat-ghost {$snapchat7}'></a>
                        </div>
               		</div>
            	</div>
            	<div class='{$class} {$member_8} team'>
            	    <img src='{$imgSrc8}' class='img-responsive teamImg'>
            	        <h4 class='text'>HUMAN RESOURCES</h4>
            	    <div class='teamDiv'>
            	        <h2>{$title8}</h2>
            	        <p>{$content8}</p>
                        <div class='fa-icons'>
                            <a href='https://www.facebook.com/{$facebook_username8}' class='facss fa-facebook {$facebook8}'></a>
                            <a href='https://twitter.com/{$twitter_username8}' class='facss fa-twitter {$twitter8}'></a>
                            <a href='https://www.linkedin.com/{$linkedin_username8}' class='facss fa-linkedin {$linkedin8}'></a>
                            <a href='https://www.youtube.com/{$youtube_username8}' class='facss fa-youtube {$youtube8}'></a>
                            <a href='https://www.instagram.com/{$instagram_username8}' class='facss fa-instagram {$instagram8}'></a>
                            <a href='https://www.snapchat.com/{$snapchat_username8}' class='facss fa-snapchat-ghost {$snapchat8}'></a>
                        </div>
                	</div>
            	</div>
                <div class='{$class} {$member_9} team'>
                    <img src='{$imgSrc9}' class='img-responsive teamImg'>
                        <h4 class='text'>HUMAN RESOURCES</h4>
                    <div class='teamDiv'>
                        <h2>{$title9}</h2>
                        <p>{$content9}</p>
                        <div class='fa-icons'>
                            <a href='https://www.facebook.com/{$facebook_username9}' class='facss fa-facebook {$facebook9}'></a>
                            <a href='https://twitter.com/{$twitter_username9}' class='facss fa-twitter {$twitter9}'></a>
                            <a href='https://www.linkedin.com/{$linkedin_username9}' class='facss fa-linkedin {$linkedin9}'></a>
                            <a href='https://www.youtube.com/{$youtube_username9}' class='facss fa-youtube {$youtube9}'></a>
                            <a href='https://www.instagram.com/{$instagram_username9}' class='facss fa-instagram {$instagram9}'></a>
                            <a href='https://www.snapchat.com/{$snapchat_username9}' class='facss fa-snapchat-ghost {$snapchat9}'></a>
                        </div>
                    </div>
                </div>
                <div class='{$class} {$member_10} team'>
                    <img src='{$imgSrc10}' class='img-responsive teamImg'>
                        <h4 class='text'>HUMAN RESOURCES</h4>
                    <div class='teamDiv'>
                        <h2>{$title10}</h2>
                        <p>{$content10}</p>
                        <div class='fa-icons'>
                            <a href='https://www.facebook.com/{$facebook_username10}' class='facss fa-facebook {$facebook10}'></a>
                            <a href='https://twitter.com/{$twitter_username10}' class='facss fa-twitter {$twitter10}'></a>
                            <a href='https://www.linkedin.com/{$linkedin_username10}' class='facss fa-linkedin {$linkedin10}'></a>
                            <a href='https://www.youtube.com/{$youtube_username10}' class='facss fa-youtube {$youtube10}'></a>
                            <a href='https://www.instagram.com/{$instagram_username10}' class='facss fa-instagram {$instagram10}'></a>
                            <a href='https://www.snapchat.com/{$snapchat_username10}' class='facss fa-snapchat-ghost {$snapchat10}'></a>
                        </div>
                    </div>
                </div>
                <div class='{$class} {$member_11} team'>
                    <img src='{$imgSrc11}' class='img-responsive teamImg'>
                        <h4 class='text'>HUMAN RESOURCES</h4>
                    <div class='teamDiv'>
                        <h2>{$title11}</h2>
                        <p>{$content11}</p>
                        <div class='fa-icons'>
                            <a href='https://www.facebook.com/{$facebook_username11}' class='facss fa-facebook {$facebook11}'></a>
                            <a href='https://twitter.com/{$twitter_username11}' class='facss fa-twitter {$twitter11}'></a>
                            <a href='https://www.linkedin.com/{$linkedin_username11}' class='facss fa-linkedin {$linkedin11}'></a>
                            <a href='https://www.youtube.com/{$youtube_username11}' class='facss fa-youtube {$youtube11}'></a>
                            <a href='https://www.instagram.com/{$instagram_username11}' class='facss fa-instagram {$instagram11}'></a>
                            <a href='https://www.snapchat.com/{$snapchat_username11}' class='facss fa-snapchat-ghost {$snapchat11}'></a>
                        </div>
                    </div>
                </div>
                <div class='{$class} {$member_12} team'>
                    <img src='{$imgSrc12}' class='img-responsive teamImg'>
                        <h4 class='text'>HUMAN RESOURCES</h4>
                    <div class='teamDiv'>
                        <h2>{$title12}</h2>
                        <p>{$content12}</p>
                        <div class='fa-icons'>
                            <a href='https://www.facebook.com/{$facebook_username12}' class='facss fa-facebook {$facebook12}'></a>
                            <a href='https://twitter.com/{$twitter_username12}' class='facss fa-twitter {$twitter12}'></a>
                            <a href='https://www.linkedin.com/{$linkedin_username12}' class='facss fa-linkedin {$linkedin12}'></a>
                            <a href='https://www.youtube.com/{$youtube_username12}' class='facss fa-youtube {$youtube12}'></a>
                            <a href='https://www.instagram.com/{$instagram_username12}' class='facss fa-instagram {$instagram12}'></a>
                            <a href='https://www.snapchat.com/{$snapchat_username12}' class='facss fa-snapchat-ghost {$snapchat12}'></a>
                        </div>
                    </div>
                </div>
        </div>";
}

add_action( 'vc_before_init', 'our_team_function' );
function our_team_function() {
   vc_map( array(
      "name" => __( "Our Team", "adaptive" ),
      "base" => "our_team",
      "class" => "",
      "category" => __( "Content", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "service-header",
            "heading" => __( "Main Title", "adaptive" ),
            "param_name" => "main_title",
            "value" => __( "Our Team", "adaptive" ),
            "description" => __( "Enter main title name.", "adaptive" ),
            'save_always' => true,
         ),
         // params group
            array(
                'type' => 'param_group',
                "heading" => __("Add team members on +", "adaptive" ),
                'value' => '',
                'param_name' => 'members',
                "description" => __( "Note: Max number of members is 12!", "adaptive" ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => 'Michael Statham',
                        'heading' => __('Enter your title', 'adaptive'),
                        'param_name' => 'title',
                        'save_always' => true,
                    ),
                    array(
                        'type' => 'textarea',
                        'value' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis dui sodales, vuliputate odi vitae, scelerisque dolor.', 'adaptive' ),
                        'heading' => __('Enter your content', 'adaptive'),
                        'param_name' => 'content',
                        'save_always' => true,
                    ),
                    array(
                        "type" => "attach_image",
                        "value" => "",
                        "heading" => __("Select Image", "js_composer"),
                        "param_name" => "image_url",
                        'save_always' => true,
                    ),
                    array(
                        "type"        => "checkbox",
                        "heading"     => __("Select social icons you want", "adaptive" ),
                        "param_name"  => "fa_icons",
                        "admin_label" => true,
                        "value"       => array(
                                            'Facebook' => 'facebook',
                                            'Twitter' => 'twitter',
                                            'Linkedin' => 'linkedin',
                                            'You tube' => 'youtube',
                                            'Instagram' => 'instagram',
                                            'Snapchat' => 'snapchat',
                                            ), //value
                        "std"         => " ",
                        "description" => __("You can select more then one.", "adaptive" ),
                        'save_always' => true,
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __('Enter your Facebook username', 'adaptive'),
                        'param_name' => 'facebook_username',
                        "dependency" => array(
                            "element" => "fa_icons",
                            'value' => 'facebook',
                        ),
                        'save_always' => true,
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __('Enter your Twitter username', 'adaptive'),
                        'param_name' => 'twitter_username',
                        "dependency" => array(
                            "element" => "fa_icons",
                            'value' => 'twitter',
                        ),
                        'save_always' => true,
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __('Enter your Linkedin username', 'adaptive'),
                        'param_name' => 'linkedin_username',
                        "dependency" => array(
                            "element" => "fa_icons",
                            'value' => 'linkedin',
                        ),
                        'save_always' => true,
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __('Enter your You tube username', 'adaptive'),
                        'param_name' => 'youtube_username',
                        "dependency" => array(
                            "element" => "fa_icons",
                            'value' => 'youtube',
                        ),
                        'save_always' => true,
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __('Enter your Instagram username', 'adaptive'),
                        'param_name' => 'instagram_username',
                        "dependency" => array(
                            "element" => "fa_icons",
                            'value' => 'instagram',
                        ),
                        'save_always' => true,
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __('Enter your Snapchat username', 'adaptive'),
                        'param_name' => 'snapchat_username',
                        "dependency" => array(
                            "element" => "fa_icons",
                            'value' => 'snapchat',
                        ),
                        'save_always' => true,
                    ),
                )
            ),
      )
   ) );
}