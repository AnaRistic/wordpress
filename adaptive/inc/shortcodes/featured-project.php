<?php

/******************************************************************************/
/* featuredProject - Shortcode */
/******************************************************************************/

add_shortcode( 'featured_project', 'featured_project_simple_shortcode' );
function featured_project_simple_shortcode( $atts, $content ) { 

   $atts = vc_map_get_attributes( 'featured_project', $atts );
    extract( $atts );
    
    if (isset($img)) {
      $img = wp_get_attachment_image_src($atts["image_url"], "large");
      $imgSrc = $img[0];
    } else {
      $imgSrc = IMAGES.'/pimgpsh_fullsize_distr.png';
    }

  
   return "<div class='row featuredProject'>

            <div class='col-md-5 col-sm-12 col-xs-12 headerFP'>
              <h4>FEATURED PROJECT</h4>
              <h1>{$title}</h1>
              <p>{$content}</p>
              <a href='' class='btn contactButton margin0' style='background-color:{$button_color}'>VIEW PROJECT</a>
            </div>
            <div class='col-md-7 vertical'>
              <img class='img-responsive' src='{$imgSrc}'>
            </div>

          </div>";
}

add_action( 'vc_before_init', 'featured_project_function' );
function featured_project_function() {
   vc_map( array(
      "name" => __( "Featured Project", "adaptive" ),
      "base" => "featured_project",
      "class" => "featuredProject",
      "category" => __( "Content", "adaptive"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/js/adaptive.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/style.css'),
      "params" => array(
         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Title", "adaptive" ),
            "param_name" => "title",
            "value" => __( "Onion Website Template for Wordpress", "adaptive" ),
            "description" => __( "Enter title name.", "adaptive" )
         ),
         array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Button background color", "adaptive" ),
            "param_name" => "button_color",
            "value" => '#ca1f5a',
            "description" => __( "Choose background button color", "adaptive" )
         ),
         array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Content", "adaptive" ),
            "param_name" => "content",
            "value" => __( "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis dui sodales, vuliputate odi vitae, scelerisque dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis dui sodales, vuliputate odi vitae, scelerisque dolor.", "adaptive" ),
            "description" => __( "Enter content.", "adaptive" )
         ),
         array(
                "type" => "attach_image",
                "heading" => __("Image", "js_composer"),
                "holder" => "div",
                "class" => "",
                "param_name" => "image_url",
                "description" => __("Your desc", "js_composer")
            ),
      )
   ) );
}