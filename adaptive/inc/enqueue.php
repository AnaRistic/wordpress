<?php

/*
	======================================
		ADMIN ENQUEUE FUNCTION
	======================================
*/
function adaptive_load_admin_scripts( $hook ) {

	if ( 'toplevel_page_anchy_adaptive' != $hook ) {
		return;
	}

	wp_register_style( 'adaptive_admin', URL . '/css/adaptive.admin.css', array(), '1.0.0', 'all' );
	wp_enqueue_style( 'adaptive_admin' );

}
add_action( 'admin_enqueue_scripts', 'adaptive_load_admin_scripts' );