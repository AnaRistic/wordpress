<footer>
    <div class="footer">
        <img src="<?php print IMAGES; ?>/logo-footer.png">
        <h3><a href="">contact@shindiristudio.com</a></h3>
        <h3>(+)381 63 255 7005</h3>
    </div>
</footer>

<?php wp_footer(); ?>

<script>
	$('.parallax-window').parallax({imageSrc: '<?php header_image(); ?>'});
</script>

</body>
</html>