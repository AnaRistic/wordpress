<?php

/*

$package adaptive
-- Archive Content

*/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('content-archive'); ?>>

	<header class="entry-header">
		<?php the_title( sprintf('<h1 class="entry-title"><a href="%s">', esc_url( get_permalink() ) ),'</a></h1>' ); ?>
		<small>Posted in <span style="width: auto; display: inline-block;"><?php the_category(); ?></span> on: <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?> by <?php the_author_link(); ?></small>
	</header>


	<div class="row">

		<?php if ( has_post_thumbnail() ) { ?>

			<div class="col-xs-12 col-sm-4">
				<div class="thumbnail"><?php the_post_thumbnail('medium'); ?></div>
			</div>

			<div class="col-xs-12 col-sm-8">
				<?php the_excerpt(); ?>
			</div>

		<?php } elseif (adaptive_get_embedded_media( array('video','iframe') )) { ?>
			<div class="col-xs-12 col-sm-6">
				<div class="embed-responsive embed-responsive-16by9">
					<?php echo adaptive_get_embedded_media( array('video','iframe') ); ?>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6">
				<?php the_excerpt(); ?>
			</div>

		<?php } else { ?>
		
			<div class="col-xs-12">
				<?php the_excerpt(); ?>
			</div>

		<?php } ?>

		<div class="button-container buttons">
			<a href="<?php the_permalink(); ?>" class="btn"><?php _e('Read more', 'adaptive'); ?></a>
		</div>

	</div>

	<footer class="enrty-footer">
		<?php echo adaptive_posted_footer(); ?>
	</footer>

</article>