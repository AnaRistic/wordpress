var slideIndex = 1;
showDivs(slideIndex);
var timedelay = 4000;

function currentDiv(n) {
	showDivs(slideIndex = n);
}

function showDivs(n) {
	var i;
	var x = document.getElementsByClassName("mySlides");
	var img = document.getElementsByClassName("images");
	var dots = document.getElementsByClassName("demo");

	if (n==undefined) {
		n = ++slideIndex
	}

	if (n > x.length) {
		slideIndex = 1
	}

	if (n < 1) {
		slideIndex = x.length
	}

	for (i = 0; i < x.length; i++) {
    	x[i].className = x[i].className.replace(" activeDiv", "");
    }

    for (i = 0; i < img.length; i++) {
    	img[i].className = img[i].className.replace(" activeDiv", "");
  	}

  	for (i = 0; i < dots.length; i++) {
  	   dots[i].className = dots[i].className.replace(" active", "");
  	}

 	x[slideIndex-1].className += " activeDiv";
 	img[slideIndex-1].className += " activeDiv";
 	dots[slideIndex-1].className += " active";

}

t = setInterval(showDivs, timedelay);


function pauseshowDivs() {
	clearInterval(t);
}

function startshowDivs() {
	t = setInterval(showDivs, timedelay);
}
