$(window).bind('scroll',function(e){
    parallaxScroll();
});

function parallaxScroll(){
    var scrolled = $(window).scrollTop();
    $('.vertical').css('top',(250-(scrolled*.25))+'px');
}

$(document).ready(function(){
    
    
    $('#scroll').click(function(e){
        e.preventDefault();

        $('html,body').animate({scrollTop: $('.container').offset().top}, 2000);


    });
});

/*$(document).ready(function() {

    $("button").click(function (){
        $('.parallax-window').animate({height: '900px'}, 100);

        if ($(".parallax-window").height() == 900) {
           $('.parallax-window').animate({height: '600px'}, 100);
           }
        else if ($(".parallax-window").height() == 600) {
           $('.parallax-window').animate({height: '900px'}, 100);
           }
        });
});*/

$(document).ready(function() {

    // contact form submission
    $('#adaptiveContactForm').submit(function(e) {

        e.preventDefault();

        $('.has-error').removeClass('has-error');
        $('.text-danger-block').removeClass('text-danger-block');
        $('.js-show-feedback').removeClass('js-show-feedback');

        var form = $(this),
            email = form.find('#email').val(),
            message = form.find('#message').val(),
            ajaxurl = form.data('url');

        if (email == '') {
            //console.log('Required inputs are empty');
            $('#email').parent('.form-group').addClass('has-error');
            $(".email-error").addClass("text-danger-block");
            return;
        }

        if (message == '') {
            //console.log('Required inputs are empty');
            $('#message').parent('.form-group').addClass('has-error');
            $(".message-error").addClass("text-danger-block");
            return;
        }

        form.find("input, button").attr("disabled", "disabled");
        $('.js-form-submission').addClass('js-show-feedback');

        $.ajax({

            url : ajaxurl,
            type : 'post',
            data : {

                email : email,
                message : message,
                action : 'adaptive_save_user_contact_form'
            },
            error : function( response ){
                $('.js-form-submission').removeClass('js-show-feedback');
                $('.js-form-error').addClass('js-show-feedback');
                form.find("input, button").removeAttr("disabled");
            },
            success : function( response ){
                console.log(response);
                if (response == 0) {

                    setTimeout(function(){
                        $('.js-form-submission').removeClass('js-show-feedback');
                        $('.js-form-error').addClass('js-show-feedback');
                        form.find("input, button").removeAttr("disabled");
                    },1000);

                } else {

                    setTimeout(function(){
                        $('.js-form-submission').removeClass('js-show-feedback');
                        $('.js-form-success').addClass('js-show-feedback');
                        form.find("input, button").removeAttr("disabled").val('');
                    },1000);

                }
            }
        });

    });

});







  