<?php

/*

@package adative
-- Video Post Format

*/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('adaptive-format-video'); ?>>

	<header class="entry-header">
		<?php the_title( sprintf('<h1 class="entry-title"><a href="%s">', esc_url( get_permalink() ) ),'</a></h1>' ); ?>
		<small>Posted in <span style="width: auto; display: inline-block;"><?php the_category(); ?></span> on: <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?> by <?php the_author_link(); ?></small>
	</header>


	<div class="entry-content">

		<div class="embed-responsive embed-responsive-16by9">
			<?php echo adaptive_get_embedded_media( array('video','iframe') ); ?>
		</div>

		<div class="button-container buttons">
			<a href="<?php the_permalink(); ?>" class="btn"><?php _e('Read more', 'adaptive'); ?></a>
		</div>

	</div>

	<footer class="enrty-footer">
		<?php echo adaptive_posted_footer(); ?>
	</footer>

</article>