<?php get_header(); ?>

<div class="container">

<?php
    
    if ( have_posts() ):

        while ( have_posts() ): the_post(); ?>

            <div class="col-xs-12 col-sm-9">

                <h3><?php the_title(); ?></h3>

            	<p><?php the_content(); ?></p>
                
            </div>

            <div class="col-xs-12 col-sm-3">
            	<?php get_sidebar(); ?>
            </div>

        <?php endwhile;

    endif;

    ?>

</div>

<?php  get_footer(); ?>