<?php get_header(); ?>

<div class="container container-center">

<div class="row">

		<div class="col-xs-12 col-sm-9">

			<?php 
    
    			if ( have_posts() ):

        			while ( have_posts() ): the_post(); ?>

    					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    						
    						<?php the_title('<h1 class="entry-title">','</h1>'); ?>

    						<?php if( has_post_thumbnail() ): ?>

    							<div class="pull-right"><?php the_post_thumbnail('thumbnail'); ?></div>

    						<?php endif; ?>

                            <small>Posted in <span style="width: auto; display: inline-block;"><?php the_category(); ?></span> on: <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?> by <?php the_author_link(); ?> | <?php edit_post_link(); ?></small>

                            <div class="clearfix">
                                <?php the_content(); ?>
                            </div>

                            <footer class="enrty-footer">
                                <?php echo adaptive_posted_footer(); ?>
                            </footer>

    						<hr>

                            <?php echo adaptive_single_post_navigation(); ?>

    						<?php
    							if( comments_open() ) { 
    								comments_template();
    							} else {
    								echo "<h5 class='text-center'>Sorry, Comments are closed!</h5>";
    							}
    						?>

    					</article>

        			<?php endwhile;

    			endif;

			?>

		</div>

		<div class="col-xs-12 col-sm-3">
			<?php get_sidebar(); ?>
		</div>

	</div>

</div>

<?php get_footer(); ?>