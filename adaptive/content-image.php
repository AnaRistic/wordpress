<?php

/*

$package adaptive
-- Image Post Format

*/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('adaptive-format-image'); ?>>

	<header class="entry-header">
		<?php the_title( sprintf('<h1 class="entry-title"><a href="%s">', esc_url( get_permalink() ) ),'</a></h1>' ); ?>
		<small>Posted in <span style="width: auto; display: inline-block;"><?php the_category(); ?></span> on: <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?> by <?php the_author_link(); ?></small>
	</header>


	<div class="row">

			<div class="col-xs-12">
				<div class="thumbnail" style="border: none;"><?php the_post_thumbnail('medium'); ?></div>
			</div>

			<div class="col-xs-12 enrty-excerpt">
				<?php the_excerpt(); ?>

				<div class="button-container buttons read-more">
					<a href="<?php the_permalink(); ?>" class="btn"><?php _e('Read more', 'adaptive'); ?></a>
				</div>
			</div>

	</div>

	<footer class="enrty-footer">
		<?php echo adaptive_posted_footer(); ?>
	</footer>

</article>