<?php get_header(); ?>

<div class="container container-center">

	<div class="row">

		<div class="col-xs-12 col-sm-9">
		
			<?php

			if ( have_posts() ):

				while ( have_posts() ): the_post(); ?>

					<?php get_template_part('content', 'search'); ?>

				<?php endwhile;

			endif;

			?>

		</div>

		<div class="col-xs-12 col-sm-3">
			<?php get_sidebar(); ?>
		</div>
	
	</div>

</div>

<?php get_footer(); ?>