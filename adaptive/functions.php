<?php


/******************************************************************************/
/* Define Constants */
/******************************************************************************/
define('THEMEROOT', get_stylesheet_directory_uri());
define('URL', get_template_directory_uri());
define('GTD', get_template_directory());
define('IMAGES', THEMEROOT. '/images');

require GTD . '/inc/function-admin.php';
require GTD . '/inc/custom-post-type.php';
require GTD . '/inc/enqueue.php';
require GTD . '/inc/ajax.php';
require GTD . '/inc/theme-support.php';
require GTD . '/inc/shortcodes/contact.php';
require GTD . '/inc/shortcodes/featured-project.php';
require GTD . '/inc/shortcodes/our-services.php';
require GTD . '/inc/shortcodes/our-team.php';
require GTD . '/inc/shortcodes/purchase.php';
require GTD . '/inc/shortcodes/social-icons.php';


/******************************************************************************/
/* Load css and js files */
/******************************************************************************/
function load_custom_scripts() {
    //css
    wp_enqueue_style( 'bootstrapcss',  THEMEROOT . '/css/bootstrap.min.css', array(), '3.3.7', 'all' );
    wp_enqueue_style( 'customstyle',  THEMEROOT . '/css/adaptive.css', array(), '1.0.0', 'all' );
    wp_enqueue_style( 'customstyleTextInputEffects',  THEMEROOT . '/css/TextInputEffects/css/set1.css', array(), '1.0.0', 'all' );
    wp_enqueue_style( 'style',  THEMEROOT . '/style.css', array(), '1.0.0', 'all' );
    wp_enqueue_style( 'socialicons',  'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', array(), '2.3.2', 'all' );

    //js
    wp_enqueue_script('bootstrapjs', THEMEROOT . '/js/bootstrap.min.js', array(), '3.3.7', true);
    wp_enqueue_script('jquery_parallax', THEMEROOT . '/js/parallax.js-1.4.2/parallax.js', array(), '1.4.2', true);
    wp_enqueue_script('customjs', THEMEROOT . '/js/adaptive.js', array(), '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'load_custom_scripts');


/******************************************************************************/
/* Manus */
/******************************************************************************/
function register_my_menus() {
	register_nav_menus(array(
		'top-menu' => __('Top Menu', 'adaptive-framework'),
	));
}
add_action('init', 'register_my_menus');


/******************************************************************************/
/* Allow SVG Images */
/******************************************************************************/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'images/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


/******************************************************************************/
/* Head function */
/******************************************************************************/
function adaptive_remove_version() {
    return '';
}
add_filter( 'the_generator', 'adaptive_remove_version' );















?>
