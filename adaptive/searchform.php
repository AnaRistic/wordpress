<form role="search" method="get" action="<?php echo home_url('/'); ?>">
	<div class="input-group">
		<input type="search" class="form-control" placeholder="Search" value="<?php get_search_query() ?>" name="s" title="Search">
		<div class="input-group-btn">
    		<button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
    	</div>
    </div>
</form>