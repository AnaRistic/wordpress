<?php /*

@package adaptivetheme

*/

if ( ! is_active_sidebar( 'adaptive-sidebar' )) {
	return;
}

?>

<div id="sidebar" class="widgets-area">
	<?php dynamic_sidebar('adaptive-sidebar'); ?>
</div>