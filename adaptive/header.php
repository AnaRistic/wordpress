<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php bloginfo('name'); ?><?php wp_title('|'); ?></title>
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<meta name="author" content="">

	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

	<!-- Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=PT Sans' rel='stylesheet'>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div class="parallax-window" data-parallax="scroll" data-image-src="<?php header_image(); ?>" >

	<div class="layer"></div>

	<nav class="navbar navbar-inverse" id="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" id="menuButton">
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
      			</button>
				<?php the_custom_logo(); ?>
    		</div>
    		<div class="collapse navbar-collapse" id="myNavbar">
      			<?php wp_nav_menu(array(
					'theme_location' => 'top-menu',
				)); ?>
    		</div>
 		 </div>
	</nav>

    <div class="text-center parallax">
        <h1><a href="<?php echo home_url(); ?>" id="blogname" style="color: #<?php header_textcolor(); ?>"><?php bloginfo('name'); ?></a></h1>
        <p id="blogtagline" style="color: #<?php header_textcolor(); ?>"><?php bloginfo('description'); ?></p>
        <a id="scroll" href="#"><img src="<?php print IMAGES; ?>/scroll-indicator.png" alt="Scroll Indicator"></a>
    </div>

</div>


