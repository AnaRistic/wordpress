<?php 

/*
	Template Name: Home page
*/


get_header(); ?>

<div class="container home-container">

<?php

	if (!empty($_GET['msg']) && $_GET['msg'] == 'success') {
        echo '<div class="alert alert-success">
                <h2><strong>Success!</strong> Message is sent.</h2>
            </div>';
    }
    
    if ( have_posts() ):

        while ( have_posts() ): the_post(); ?>

            <?php the_content(); ?>

        <?php endwhile;

    endif;

    ?>

</div>

<?php get_footer(); ?>