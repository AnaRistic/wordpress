<?php get_header(); ?>

<div class="container">

	<div class="row">

		<div class="col-xs-12 col-sm-9">

			<?php 
    
    			if ( have_posts() ):

        			while ( have_posts() ): the_post(); ?>

    					<?php get_template_part('content', get_post_format()); ?>

        			<?php endwhile; ?>

                    <hr>

                    <footer class="enrty-footer">
                        <?php echo adaptive_post_navigation(); ?>
                    </footer>

        			<!--<div class="col-xs-6 text-left pagination buttons">
        				<?php //next_posts_link('&laquo; Older Posts'); ?>
        			</div>
        			<div class="col-xs-6 text-right pagination buttons">
        				<?php //previous_posts_link('Newer Posts &raquo;'); ?>
        			</div>-->

    			<?php endif;

			?>

		</div>

		<div class="col-xs-12 col-sm-3">
			<?php get_sidebar(); ?>
		</div>

	</div>

</div>

<?php get_footer(); ?>