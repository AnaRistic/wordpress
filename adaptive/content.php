<?php

/*

$package adaptive
-- Stanard Post Format

*/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php the_title( sprintf('<h1 class="entry-title"><a href="%s">', esc_url( get_permalink() ) ),'</a></h1>' ); ?>
		<small>Posted in <span style="width: auto; display: inline-block;"><?php the_category(); ?></span> on: <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?> by <?php the_author_link(); ?></small>
	</header>


	<div class="row">

		<?php if ( has_post_thumbnail() ): ?>

			<div class="col-xs-12 col-sm-4">
				<div class="thumbnail"><?php the_post_thumbnail('medium'); ?></div>
			</div>

			<div class="col-xs-12 col-sm-8">
				<?php the_excerpt(); ?>
			</div>

		<?php else: ?>
		
			<div class="col-xs-12">
				<?php the_excerpt(); ?>
			</div>

		<?php endif; ?>

		<div class="button-container buttons">
			<a href="<?php the_permalink(); ?>" class="btn" style="margin-left: 25px;"><?php _e('Read more', 'adaptive'); ?></a>
		</div>

	</div>

	<footer class="enrty-footer">
		<?php echo adaptive_posted_footer(); ?>
	</footer>

</article>